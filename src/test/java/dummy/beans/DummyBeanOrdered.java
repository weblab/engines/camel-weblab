/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package dummy.beans;

import java.net.URI;

import org.ow2.weblab.annotation.WebLabProperty;
import org.ow2.weblab.annotation.WebLabResource;


/**
 * A dummy annotated bean to test the bean parser.
 *
 * @author emilienbondu
 *
 */
@WebLabResource(value=WebLabResource.DOCUMENT, resourceIdList={"audio", "image", "text"})
public class DummyBeanOrdered {


	@WebLabResource(value=WebLabResource.TEXT, id="text")
	private String textContent;

	@WebLabResource(value=WebLabResource.AUDIO, id="audio")
	private URI audioContent;


	@WebLabResource(value=WebLabResource.IMAGE, id="image")
	private byte[] imageContent;

	@WebLabProperty(value="dc", subjectId="image")
	private String format = "image/jpg";


	public String getTextContent() {
		return this.textContent;
	}


	public void setTextContent(String textContent) {
		this.textContent = textContent;
	}


	public URI getAudioContent() {
		return this.audioContent;
	}


	public void setAudioContent(URI audioContent) {
		this.audioContent = audioContent;
	}

	public byte[] getImageContent() {
		return this.imageContent;
	}


	public void setImageContent(byte[] imageContent) {
		this.imageContent = imageContent;
	}


	public String getFormat() {
		return this.format;
	}


	public void setFormat(String format) {
		this.format = format;
	}
}