/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package dummy.beans;

import java.net.URI;

import org.ow2.weblab.annotation.WebLabProperty;
import org.ow2.weblab.annotation.WebLabResource;

/**
 * A dummy bean that has 3 audio media unit.
 * 2 are normalized content of the first media unit.
 *
 * @author VA&Iuml;SSE-LESTEVEN Arthur (arthurvaisse(at)yahoo(dot)fr)
 *
 */
@WebLabResource(value = WebLabResource.DOCUMENT, resourceIdList={"nativeContent_audio", "normalized_content_canal_left", "normalized_content_canal_right"})
public class AnnotatedMediaUnitBean {
	@WebLabProperty("dc")
	String creator = "arthur vaisse";

	@WebLabResource(value = WebLabResource.AUDIO)
	@WebLabProperty(value ="wlp", localname="hasNativeContent" )
	public URI	nativeContent_audio;

	@WebLabResource(WebLabResource.AUDIO)
	@WebLabProperty(value = "wlp", localname = "hasNormalisedContent", subjectId = "nativeContent_audio")
	public URI	normalized_content_canal_left;

	@WebLabResource(WebLabResource.AUDIO)
	@WebLabProperty(value = "wlp", localname = "hasNormalisedContent", subjectId = "nativeContent_audio")
	public URI	normalized_content_canal_right;

	public URI getNativeContent_audio() {
		return this.nativeContent_audio;
	}

	public URI getNormalized_content_canal_left() {
		return this.normalized_content_canal_left;
	}

	public URI getNormalized_content_canal_right() {
		return this.normalized_content_canal_right;
	}

	public void setNativeContent_audio(URI nativeContent_audio) {
		this.nativeContent_audio = nativeContent_audio;
	}

	public void setNormalized_content_canal_left(URI normalized_content_canal_left) {
		this.normalized_content_canal_left = normalized_content_canal_left;
	}

	public void setNormalized_content_canal_right(URI normalized_content_canal_right) {
		this.normalized_content_canal_right = normalized_content_canal_right;
	}

	@Override
	public String toString() {
		return "AnnotatedMediaUnitBean [nativeContent_audio=" + this.nativeContent_audio + ", normalized_content_canal_left=" + this.normalized_content_canal_left
				+ ", normalized_content_canal_right=" + this.normalized_content_canal_right + "]";
	}

}
