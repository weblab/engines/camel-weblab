/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package dummy.beans;

import java.net.URI;

import org.ow2.weblab.annotation.WebLabProperty;
import org.ow2.weblab.annotation.WebLabResource;


/**
 * A dummy annotated bean to test the bean parser.
 *
 * @author emilienbondu
 *
 */
@WebLabResource(value=WebLabResource.DOCUMENT)
public class DummyBeanWithAnnotations {


	@WebLabResource(value=WebLabResource.TEXT, id="text")
	private String textContent;

	@WebLabResource(value=WebLabResource.AUDIO, id="audioContent")
	private URI audioContent;



	@WebLabProperty(value = "wlp", localname = "hasNormalisedContent", subjectId = "audioContent")
	private URI normalizedContent;


	public String getTextContent() {
		return this.textContent;
	}


	public void setTextContent(String textContent) {
		this.textContent = textContent;
	}


	public URI getAudioContent() {
		return this.audioContent;
	}


	public void setAudioContent(URI audioContent) {
		this.audioContent = audioContent;
	}


	public URI getNormalizedContent() {
		return this.normalizedContent;
	}


	public void setNormalizedContent(URI normalizedContent) {
		this.normalizedContent = normalizedContent;
	}
}