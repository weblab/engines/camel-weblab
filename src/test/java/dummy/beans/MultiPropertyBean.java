/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package dummy.beans;

import org.ow2.weblab.annotation.WebLabProperty;
import org.ow2.weblab.annotation.WebLabResource;

/**
 * @author VA&Iuml;SSE-LESTEVEN Arthur (arthurvaisse(at)yahoo(dot)fr)
 *
 */
@WebLabResource(WebLabResource.DOCUMENT)
public class MultiPropertyBean {

	@WebLabResource(WebLabResource.TEXT)
	private String textContent="I'm the happy content of a media unit text :)";

	//Simple multiproperty that only uses know prefixes.
	@WebLabProperty(value = { "dc", "dc", "wlp" },localname = { "publisher", "contributor", "hasOriginalFileName" })
	private String name = "Arthur Vaisse-Lesteven";

	@WebLabResource(WebLabResource.TEXT)
	@WebLabProperty(value = {"wlp", "wlp"}, localname = {"hasNormalisedContent", "isCleansingOf"}, subjectId="textContent")
	private String secondtextContent="I'm the second almost happy content of a media unit text :s. One day I wanna de the first!";

	public String getTextContent() {
		return this.textContent;
	}

	public void setTextContent(String textContent) {
		this.textContent = textContent;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecondtextContent() {
		return this.secondtextContent;
	}

	public void setSecondtextContent(String secondtextContent) {
		this.secondtextContent = secondtextContent;
	}
}
