/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package dummy.beans;

import java.net.URI;
import java.util.List;

import org.ow2.weblab.annotation.WebLabProperty;
import org.ow2.weblab.annotation.WebLabResource;
import org.ow2.weblab.annotation.WebLabSegment;


/**
 * A dummy annotated bean to test the bean parser.
 *
 * @author emilienbondu
 *
 */
@WebLabResource(WebLabResource.DOCUMENT)
public class DummyBean {

	@WebLabProperty(value = "dc", localname="title")
	private String title;

	@WebLabProperty("dc")
	private String dummyNotdefined;

	@WebLabProperty("dc")
	private List<String> creator;

	@WebLabProperty(value = "dc", localname="subject", lang="en")
	private String subject_en;

	@WebLabProperty(value = "dc", localname="subject", lang="fr")
	private String subject_fr;

	@WebLabProperty("dc")
	private String format = "html/text"; // default value

	@WebLabProperty(value="wlp", localname="canBeIgnored")
	private boolean ignored;

	@WebLabProperty(
			value={"dc","wlp"},
			property={"http://dublincore/","weblab://prop/"},
			localname={"multi","multi1"})
	private long multi;

	@WebLabResource(WebLabResource.AUDIO)
	private URI notDefinedAudio;

	@WebLabResource(id="image1", value=WebLabResource.IMAGE)
	private URI image;

	@WebLabResource(WebLabResource.IMAGE)
	private URI myimg;

	// annotat the previous image
	@WebLabProperty(value="myio", property="http://my/image/onto/1.1/", localname="test", subjectId="myimg")
	private String imageFeature;

	@WebLabResource(value=WebLabResource.TEXT, writeContent=true)
	private String text;

	@WebLabProperty(value="dc", localname="author")
	private List<String> list;

	@WebLabProperty(value="wlp", localname="ioi")
	@WebLabSegment(value = WebLabSegment.LINEARSEGMENT, mediaUnitId="text")
	private int[] intervals;

	private SubText subtext;

	private List<SubImage> subimages;

	@WebLabResource(WebLabResource.IMAGE)
	public static class SubImage{

		@WebLabProperty("dc")
		private String test = "infoImage";

		private URI content;

		// getters and setters
		public URI getContent() {
			return this.content;
		}

		public void setContent(URI content) {
			this.content = content;
		}

		public String getTest() {
			return this.test;
		}
		public void setTest(String test) {
			this.test = test;
		}
	}

	@WebLabResource(value=WebLabResource.TEXT, writeContent= true)
	public static class SubText{

		@WebLabProperty("dc")
		private String meta = "infoText";

		@WebLabProperty(value="wlp",localname="hash")
		@WebLabSegment(WebLabSegment.LINEARSEGMENT)
		private int[] intervals;

		private String content;

		// getters and setters

		public String getContent() {
			return this.content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		public String getMeta() {
			return this.meta;
		}

		public void setMeta(String meta) {
			this.meta = meta;
		}

		public int[] getIntervals() {
			return this.intervals;
		}

		public void setIntervals(int[] intervals) {
			this.intervals = intervals;
		}
	}

	// getters and setters

	public List<String> getCreator() {
		return this.creator;
	}

	public void setCreator(List<String> creator) {
		this.creator = creator;
	}

	public String getFormat() {
		return this.format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getSubject_fr() {
		return this.subject_fr;
	}

	public void setSubject_fr(String subject_fr) {
		this.subject_fr = subject_fr;
	}

	public URI getMyimg() {
		return this.myimg;
	}

	public void setMyimg(URI myimg) {
		this.myimg = myimg;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImageFeature() {
		return this.imageFeature;
	}

	public void setImageFeature(String imageFeature) {
		this.imageFeature = imageFeature;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isIgnored() {
		return this.ignored;
	}

	public void setIgnored(boolean ignored) {
		this.ignored = ignored;
	}

	public long getMulti() {
		return this.multi;
	}

	public void setMulti(long multi) {
		this.multi = multi;
	}

	public String getSubject_en() {
		return this.subject_en;
	}

	public void setSubject_en(String subject_en) {
		this.subject_en = subject_en;
	}

	public SubText getSubtext() {
		return this.subtext;
	}

	public void setSubtext(SubText subtext) {
		this.subtext = subtext;
	}

	public URI getNotDefinedAudio() {
		return this.notDefinedAudio;
	}

	public void setNotDefinedAudio(URI notDefinedAudio) {
		this.notDefinedAudio = notDefinedAudio;
	}

	public String getDummyNotdefined() {
		return this.dummyNotdefined;
	}

	public void setDummyNotdefined(String dummyNotdefined) {
		this.dummyNotdefined = dummyNotdefined;
	}

	public URI getImage() {
		return this.image;
	}

	public void setImage(URI image) {
		this.image = image;
	}

	public List<String> getList() {
		return this.list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	public int[] getIntervals() {
		return this.intervals;
	}

	public void setIntervals(int[] intervals) {
		this.intervals = intervals;
	}

	public List<SubImage> getSubimages() {
		return this.subimages;
	}

	public void setSubimages(List<SubImage> subimages) {
		this.subimages = subimages;
	}
}