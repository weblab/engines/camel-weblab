/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.model.extended;

import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.services.AccessDeniedException;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.Trainable;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;
import org.ow2.weblab.core.services.trainable.AddTrainResourceArgs;
import org.ow2.weblab.core.services.trainable.AddTrainResourceReturn;
import org.ow2.weblab.core.services.trainable.ResetTrainedModelArgs;
import org.ow2.weblab.core.services.trainable.ResetTrainedModelReturn;
import org.ow2.weblab.core.services.trainable.TrainArgs;
import org.ow2.weblab.core.services.trainable.TrainReturn;
import org.ow2.weblab.engine.camel.WeblabProducer;

/**
 * A dummy trainable class that implement the {@link Trainable trainable} interface and do nothing. This is a test-purpose class.
 *
 * @author Vaisse-Lesteven Artthur
 */
@WebService
public class DummyTrainable implements Trainable {

	Log logger = LogFactory.getLog(WeblabProducer.class);

	@Override
	public AddTrainResourceReturn addTrainResource(final AddTrainResourceArgs args) throws AccessDeniedException, ContentNotAvailableException,
			InsufficientResourcesException, InvalidParameterException, ServiceNotConfiguredException, UnexpectedException, UnsupportedRequestException {
		this.logger.debug("DummyTrainable - addTrainResource method called.");

		assert(args.getResource() != null);

		return new AddTrainResourceReturn();
	}

	@Override
	public ResetTrainedModelReturn resetTrainedModel(final ResetTrainedModelArgs args) throws AccessDeniedException, ContentNotAvailableException,
			InsufficientResourcesException, InvalidParameterException, ServiceNotConfiguredException, UnexpectedException, UnsupportedRequestException {
		this.logger.debug("DummyTrainable - resetTrainedModel method called.");
		return new ResetTrainedModelReturn();
	}

	@Override
	public TrainReturn train(final TrainArgs args) throws AccessDeniedException, ContentNotAvailableException, InsufficientResourcesException,
			InvalidParameterException, ServiceNotConfiguredException, UnexpectedException, UnsupportedRequestException {
		this.logger.debug("DummyTrainable - train method called.");
		return new TrainReturn();
	}
}
