/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.core.model.extended;

import java.util.HashMap;
import java.util.Map;

import javax.jws.WebService;

import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.AccessDeniedException;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ResourceContainer;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;
import org.ow2.weblab.core.services.resourcecontainer.LoadResourceArgs;
import org.ow2.weblab.core.services.resourcecontainer.LoadResourceReturn;
import org.ow2.weblab.core.services.resourcecontainer.SaveResourceArgs;
import org.ow2.weblab.core.services.resourcecontainer.SaveResourceReturn;

/**
 * A dummy resource container.
 * @author ebondu
 *
 */
@WebService
public class DummyResourceContainer implements ResourceContainer {

	private Map<String, Resource> repo = new HashMap<>();

	@Override
	public SaveResourceReturn saveResource(SaveResourceArgs args) throws UnexpectedException, AccessDeniedException,
			UnsupportedRequestException, ServiceNotConfiguredException, InvalidParameterException,
			InsufficientResourcesException, ContentNotAvailableException {
		SaveResourceReturn ret = new SaveResourceReturn();
		ret.setResourceId(args.getResource().getUri());

		this.repo.put(args.getResource().getUri(), args.getResource());
		return ret;
	}

	@Override
	public LoadResourceReturn loadResource(LoadResourceArgs args) throws UnexpectedException, AccessDeniedException,
			UnsupportedRequestException, ServiceNotConfiguredException, InvalidParameterException,
			InsufficientResourcesException, ContentNotAvailableException {

		LoadResourceReturn ret = new LoadResourceReturn();
		ret.setResource(this.repo.get(args.getResourceId()));
		return ret;
	}
}