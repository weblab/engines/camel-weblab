/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;

import javax.imageio.ImageIO;

import org.apache.camel.CamelContext;
import org.apache.camel.CamelExecutionException;
import org.apache.camel.EndpointInject;
import org.apache.camel.NoTypeConversionAvailableException;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.Video;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:camel/TypeConversionRoutes.xml" }, name = "ResourcesConversion")
public class TypeConversionTest {


	@Autowired
	protected CamelContext camelContext;


	@EndpointInject(uri = "mock:resultText")
	protected MockEndpoint mockText;


	@EndpointInject(uri = "mock:resultImage")
	protected MockEndpoint mockImage;


	@EndpointInject(uri = "mock:resultAudio")
	protected MockEndpoint mockAudio;


	@EndpointInject(uri = "mock:resultVideo")
	protected MockEndpoint mockVideo;


	@EndpointInject(uri = "mock:resultDocument")
	protected MockEndpoint mockDocument;


	@Test
	public void testDocumentNativeContentConversion() throws Exception {
		// create some contents
		String text = "New Document !";
		String streamedText = "New Document from stream";
		String textInFile = "Document from file";
		String textInBAOS = "Document in BAOS";

		File file = File.createTempFile("todelete", "tmp");
		try (FileWriter fwriter = new FileWriter(file)) {
			fwriter.write(textInFile);
		}

		// wrapping
		try (InputStream is = IOUtils.toInputStream(streamedText, StandardCharsets.UTF_8)) {

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			baos.write(textInBAOS.getBytes());

			// test Document creation with a String
			this.camelContext.createProducerTemplate().sendBody("direct:createDocumentWithNativeContent", text);

			// test Document creation with an inputstream
			this.camelContext.createProducerTemplate().sendBody("direct:createDocumentWithNativeContent", is);

			// test Document creation with a file
			this.camelContext.createProducerTemplate().sendBody("direct:createDocumentWithNativeContent", file);

			// test Document creation with a BAOS
			this.camelContext.createProducerTemplate().sendBody("direct:createDocumentWithNativeContent", baos);

			// check results
			this.mockDocument.expectedMessageCount(4);
			this.mockDocument.assertIsSatisfied();
			ContentManager cm = ContentManager.getInstance();

			Object body = this.mockDocument.getReceivedExchanges().get(0).getIn().getBody();
			Assert.assertTrue(body instanceof Document);
			WProcessingAnnotator wpa = new WProcessingAnnotator((Document) body);
			URI uri = wpa.readNativeContent().firstTypedValue();
			Assert.assertNotNull(uri);

			// check content
			File contentFile = cm.readLocalExistingFile(new WProcessingAnnotator((Document) body).readNativeContent().firstTypedValue(), null, null);
			Assert.assertNotNull(contentFile);
			Assert.assertEquals(text, FileUtils.readFileToString(contentFile, StandardCharsets.UTF_8));
			// ok we delete the temporary native content
			contentFile.delete();

			body = this.mockDocument.getReceivedExchanges().get(1).getIn().getBody();
			Assert.assertTrue(body instanceof Document);
			wpa = new WProcessingAnnotator((Document) body);
			uri = wpa.readNativeContent().firstTypedValue();
			Assert.assertNotNull(uri);

			// check content
			contentFile = cm.readLocalExistingFile(new WProcessingAnnotator((Document) body).readNativeContent().firstTypedValue(), null, null);
			Assert.assertNotNull(contentFile);
			Assert.assertEquals(streamedText, FileUtils.readFileToString(contentFile, StandardCharsets.UTF_8));
			// ok we delete the temporary native content
			contentFile.delete();

			body = this.mockDocument.getReceivedExchanges().get(2).getIn().getBody();
			Assert.assertTrue(body instanceof Document);
			wpa = new WProcessingAnnotator((Document) body);
			uri = wpa.readNativeContent().firstTypedValue();
			Assert.assertNotNull(uri);

			// check content
			contentFile = cm.readLocalExistingFile(new WProcessingAnnotator((Document) body).readNativeContent().firstTypedValue(), null, null);
			Assert.assertNotNull(contentFile);
			Assert.assertEquals(textInFile, FileUtils.readFileToString(contentFile, StandardCharsets.UTF_8));
			// ok we delete the temporary native content
			contentFile.delete();

			body = this.mockDocument.getReceivedExchanges().get(3).getIn().getBody();
			Assert.assertTrue(body instanceof Document);
			wpa = new WProcessingAnnotator((Document) body);
			uri = wpa.readNativeContent().firstTypedValue();
			Assert.assertNotNull(uri);

			// check content
			contentFile = cm.readLocalExistingFile(new WProcessingAnnotator((Document) body).readNativeContent().firstTypedValue(), null, null);
			Assert.assertNotNull(contentFile);
			Assert.assertEquals(textInBAOS, FileUtils.readFileToString(contentFile, StandardCharsets.UTF_8));
			// ok we delete the temporary native content
			contentFile.delete();

			file.delete();
		}
	}


	@Test
	public void testVideoConversion() throws Exception {
		String dummyData = "dummy DATA Video";

		// TODO: add a test with a randomly generated video such as the generated image below

		this.camelContext.createProducerTemplate().sendBody("direct:createVideoWithBody", dummyData);
		this.camelContext.createProducerTemplate().sendBody("direct:createVideoWithBody", dummyData.getBytes());

		// check results
		this.mockVideo.expectedMessageCount(2);
		this.mockVideo.assertIsSatisfied();
		Object body = this.mockVideo.getReceivedExchanges().get(0).getIn().getBody();
		Assert.assertTrue(body instanceof Video);
		Assert.assertArrayEquals(dummyData.getBytes(), ((Video) body).getContent());
		body = this.mockVideo.getReceivedExchanges().get(1).getIn().getBody();
		Assert.assertTrue(body instanceof Video);
		Assert.assertArrayEquals(dummyData.getBytes(), ((Video) body).getContent());
	}


	@Test
	public void testAudioConversion() throws Exception {
		String dummyData = "dummy DATA Audio";

		// TODO: add a test with a randomly generated video such as the generated image below

		this.camelContext.createProducerTemplate().sendBody("direct:createAudioWithBody", dummyData);
		this.camelContext.createProducerTemplate().sendBody("direct:createAudioWithBody", dummyData.getBytes());

		// check results
		this.mockAudio.expectedMessageCount(2);
		this.mockAudio.assertIsSatisfied();
		Object body = this.mockAudio.getReceivedExchanges().get(0).getIn().getBody();
		Assert.assertTrue(body instanceof Audio);
		Assert.assertArrayEquals(dummyData.getBytes(), ((Audio) body).getContent());
		body = this.mockAudio.getReceivedExchanges().get(1).getIn().getBody();
		Assert.assertTrue(body instanceof Audio);
		Assert.assertArrayEquals(dummyData.getBytes(), ((Audio) body).getContent());
	}


	@Test
	public void testImageConversion() throws Exception {
		// create some contents
		int size = 100;
		BufferedImage image = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
				Color color = new Color((int) Math.floor(255 * Math.random()), (int) Math.floor(255 * Math.random()), (int) Math.floor(255 * Math.random()));
				image.setRGB(x, y, color.getRGB());
			}
		}

		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			ImageIO.write(image, "png", baos);
			String test = "test";

			this.camelContext.createProducerTemplate().sendBody("direct:createImageWithBody", baos);

			this.camelContext.createProducerTemplate().sendBody("direct:createImageWithBody", test);

			// check results
			this.mockImage.expectedMessageCount(2);
			this.mockImage.assertIsSatisfied();
			Object body = this.mockImage.getReceivedExchanges().get(0).getIn().getBody();
			Assert.assertTrue(body instanceof Image);
			Assert.assertArrayEquals(baos.toByteArray(), ((Image) body).getContent());
			body = this.mockImage.getReceivedExchanges().get(1).getIn().getBody();
			Assert.assertTrue(body instanceof Image);
			Assert.assertArrayEquals(test.getBytes(), ((Image) body).getContent());
		}
	}


	@Test
	public void testTextConversion() throws Exception {

		// create some contents
		String text = "New Text !";
		String streamedText = "New Text from stream";
		String textInFile = "Text from file";
		String textInBAOS = "Text in BAOS";
		double randomValue = Math.random();

		File file = File.createTempFile("todelete", "tmp");
		try (FileWriter fwriter = new FileWriter(file)) {
			fwriter.write(textInFile);
		}

		// wrapping
		try (InputStream is = IOUtils.toInputStream(streamedText, StandardCharsets.UTF_8)) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			baos.write(textInBAOS.getBytes());

			// test Text creation with a String
			this.camelContext.createProducerTemplate().sendBody("direct:createTextWithBody", text);

			// test Text creation with an inputstream
			this.camelContext.createProducerTemplate().sendBody("direct:createTextWithBody", is);

			// test Text creation with a file
			this.camelContext.createProducerTemplate().sendBody("direct:createTextWithBody", file);

			// test Text creation with null value
			// it will create a text with an empty content
			this.camelContext.createProducerTemplate().sendBody("direct:createTextWithBody", null);

			// test Text creation with a BAOS
			this.camelContext.createProducerTemplate().sendBody("direct:createTextWithBody", baos);

			// test Text creation with a random double
			try {
				this.camelContext.createProducerTemplate().sendBody("direct:createTextWithBody", Double.valueOf(randomValue));
				Assert.fail("Converters do not support Double to String conversion");
			} catch (CamelExecutionException cee) {
				// make sure that the missing typeConverter caused the exception
				Assert.assertEquals(NoTypeConversionAvailableException.class, cee.getCause().getClass());
			}

			// check results
			this.mockText.expectedMessageCount(5);
			this.mockText.assertIsSatisfied();
			Object body = this.mockText.getReceivedExchanges().get(0).getIn().getBody();
			Assert.assertTrue(body instanceof Text);
			Assert.assertEquals(text, ((Text) body).getContent());
			body = this.mockText.getReceivedExchanges().get(1).getIn().getBody();
			Assert.assertTrue(body instanceof Text);
			Assert.assertEquals(streamedText, ((Text) body).getContent());
			body = this.mockText.getReceivedExchanges().get(2).getIn().getBody();
			Assert.assertTrue(body instanceof Text);
			Assert.assertEquals(textInFile, ((Text) body).getContent());
			body = this.mockText.getReceivedExchanges().get(3).getIn().getBody();
			Assert.assertTrue(body instanceof Text);
			Assert.assertEquals("", ((Text) body).getContent());
			body = this.mockText.getReceivedExchanges().get(4).getIn().getBody();
			Assert.assertTrue(body instanceof Text);
			Assert.assertEquals(textInBAOS, ((Text) body).getContent());
		}

		file.delete();

	}

}
