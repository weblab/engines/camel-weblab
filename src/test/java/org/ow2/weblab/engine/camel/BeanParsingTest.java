/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel;

import java.io.File;
import java.io.FileInputStream;
import java.io.StringReader;
import java.net.URI;
import java.util.Arrays;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.rdf.Value;
import org.purl.dc.elements.DublinCoreAnnotator;

import dummy.beans.AnnotatedMediaUnitBean;
import dummy.beans.DummyBean;
import dummy.beans.DummyBean.SubText;
import dummy.beans.DummyBeanOrdered;
import dummy.beans.DummyBeanWithAnnotations;
import dummy.beans.MultiPropertyBean;

public class BeanParsingTest {

	@Test
	public void testCreateWLFromBean() throws Exception{

		CamelContext context = new DefaultCamelContext();

		final WebLabBeanParser ap = new WebLabBeanParser();
		ap.setDestinationClass(DummyBean.class.getName());

		context.addRoutes(new RouteBuilder() {
			@Override
			public void configure() throws Exception {
				// Bean to WebLab Document
				from("direct:bean2weblab").to("weblab:create?type=Document&outputMethod=xml");

				// WebLab Document to Bean
				from("direct:weblab2bean").transform(ap);

				from("direct:textbean2enrich").process(new Processor() {

					@Override
					public void process(Exchange exchange) throws Exception {
						SubText st = new SubText();
						st.setContent("my enriched content");
						st.setMeta("some things of interest !");

						exchange.getIn().setBody(st);
					}
				});

				from("direct:docbean2enrich").process(new Processor() {

					@Override
					public void process(Exchange exchange) throws Exception {
						DummyBean dbe = new DummyBean();

						dbe.setCreator(Arrays.asList("john"));
						dbe.setFormat("my-appli");
						dbe.setSubject_fr("Mon sujet");

						exchange.getIn().setBody(dbe);
					}
				});

				// Enrich WebLab Document with a text bean
				from("direct:enrichWeblabWithText").enrich("direct:textbean2enrich", ap);

				// Enrich WebLab Document with a Document bean
				from("direct:enrichWeblabWithDoc").enrich("direct:docbean2enrich", ap);
			}
		});
		context.start();

		ProducerTemplate pt = context.createProducerTemplate();
		DummyBean db = new DummyBean();
//		db.image = URI.create("http://weblab-project.org/resources/assets/licenses/gnu-fdl.png");
		db.setMyimg(new File("src/test/resources/img.png").toURI());
		db.setTitle("mytitle");
		db.setImageFeature("myfeat");
		db.setText("my super text");
		db.setMulti(10l);
		db.setSubject_en("My subject");
		db.setCreator(Arrays.asList("moi","lui","nous"));
		db.setIgnored(true);

		db.setSubtext(new SubText());
		db.getSubtext().setContent("test 123456 snap");
		db.getSubtext().setMeta("my-metinfo");
		db.getSubtext().setIntervals(new int[]{0,4,5,11,12,16});

		// test document bean to WebLab document transform
		String result = pt.requestBody("direct:bean2weblab", db, String.class);

		WebLabMarshaller m = new WebLabMarshaller();
		Document doc = m.unmarshal(new StringReader(result), Document.class);

		// 2 text, 1 image
		Assert.assertEquals(3, doc.getMediaUnit().size());
		Assert.assertEquals(1, doc.getAnnotation().size());

		DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);

		Assert.assertArrayEquals(db.getCreator().toArray(), dca.readCreator().getValues().toArray());
		Assert.assertEquals(db.getTitle(), dca.readTitle().firstTypedValue());

		Assert.assertEquals(1, doc.getMediaUnit().get(0).getAnnotation().size());
		Assert.assertEquals(3, doc.getMediaUnit().get(0).getSegment().size());

		WProcessingAnnotator wpa = new WProcessingAnnotator(doc);
		Assert.assertEquals(Boolean.valueOf(db.isIgnored()), wpa.readCanBeIgnored().firstTypedValue());

		// TODO: complete tests asserts

		// test document to document bean transform
		DummyBean db2 = pt.requestBody("direct:weblab2bean", doc, DummyBean.class);
		Assert.assertEquals(db.getTitle(), db2.getTitle());
		Assert.assertEquals(db.getCreator(), db2.getCreator());
		Assert.assertEquals("html/text", db2.getFormat()); // default value
		Assert.assertEquals(10l, db2.getMulti());
		Assert.assertNull(db2.getText()); // does not support sub mediaunit eval, use camel-sparl instead

		// TODO: complete tests asserts

		// test document enrichment with text bean
		Document doc2 = pt.requestBody("direct:enrichWeblabWithText", doc, Document.class);

		// 3 text, 1 image
		Assert.assertEquals(4, doc2.getMediaUnit().size());
		Assert.assertEquals(1, doc2.getAnnotation().size());

		dca = new DublinCoreAnnotator(doc2);

		Assert.assertArrayEquals(db.getCreator().toArray(), dca.readCreator().getValues().toArray());
		Assert.assertEquals(db.getTitle(), dca.readTitle().firstTypedValue());

		Assert.assertEquals(1, doc.getMediaUnit().get(0).getAnnotation().size());
		Assert.assertEquals(3, doc.getMediaUnit().get(0).getSegment().size());

		Assert.assertEquals( "my enriched content", ((Text)doc2.getMediaUnit().get(3)).getContent());

		// TODO: complete tests asserts

		// test document enrichment with document bean
		Document doc3 = pt.requestBody("direct:enrichWeblabWithDoc", doc2, Document.class);

		// 3 text, 1 image
		Assert.assertEquals(4, doc3.getMediaUnit().size());
		Assert.assertEquals(1, doc3.getAnnotation().size());

		dca = new DublinCoreAnnotator(doc3);

		Assert.assertEquals(db.getCreator().size()+1, dca.readCreator().getValues().size());

		// TODO: complete tests asserts

		DummyBeanOrdered dbo = new DummyBeanOrdered();
		dbo.setTextContent("my text");
		dbo.setAudioContent(new File("src/test/resources/img.png").toURI());
		try (FileInputStream input = new FileInputStream("src/test/resources/img.png");) {
			dbo.setImageContent(IOUtils.toByteArray(input));
		}

		dbo.setFormat("image/png");

		result = pt.requestBody("direct:bean2weblab", dbo, String.class);
		doc = m.unmarshal(new StringReader(result), Document.class);

		// test order of MU
		Assert.assertTrue(doc.getMediaUnit().get(0) instanceof Audio);
		Assert.assertTrue(doc.getMediaUnit().get(1) instanceof Image);
		Assert.assertTrue(doc.getMediaUnit().get(2) instanceof Text);


		DummyBeanWithAnnotations dba = new DummyBeanWithAnnotations();
		dba.setTextContent("my text");
		dba.setAudioContent(new File("src/test/resources/img.png").toURI());
		dba.setNormalizedContent(new File("src/test/resources/img.png").toURI());

		result = pt.requestBody("direct:bean2weblab", dba, String.class);
		doc = m.unmarshal(new StringReader(result), Document.class);

		// test Annotated MU
		Assert.assertTrue(doc.getMediaUnit().get(0) instanceof Audio);


		AnnotatedMediaUnitBean amub = new AnnotatedMediaUnitBean();
		amub.setNativeContent_audio(new File("src/test/resources/audios/file3.wav").toURI());
		amub.setNormalized_content_canal_left(new File("src/test/resources/audios/file3_left.wav").toURI());
		amub.setNormalized_content_canal_right(new File("src/test/resources/audios/file3_right.wav").toURI());

		result = pt.requestBody("direct:bean2weblab", amub, String.class);

		doc = m.unmarshal(new StringReader(result), Document.class);

		Assert.assertEquals(3, doc.getMediaUnit().size());
		Assert.assertEquals(1, doc.getAnnotation().size());

		WProcessingAnnotator wlpAnot = new WProcessingAnnotator(doc);
		Assert.assertEquals(1,wlpAnot.readNativeContent().size());

		URI nativeContent = wlpAnot.readNativeContent().firstTypedValue();

		wlpAnot.startInnerAnnotatorOn(nativeContent);
		Value<URI> normalisedContent = wlpAnot.readNormalisedContent();
		Assert.assertEquals(2,normalisedContent.size());

		MultiPropertyBean mpb = new MultiPropertyBean();

		result = pt.requestBody("direct:bean2weblab", mpb, String.class);
		doc = m.unmarshal(new StringReader(result), Document.class);

		Assert.assertEquals(2, doc.getMediaUnit().size());
		Assert.assertEquals(1, doc.getAnnotation().size());

		String name = "Arthur Vaisse-Lesteven";

		dca = new DublinCoreAnnotator(doc);
		Assert.assertEquals(1,dca.readPublisher().size());
		Assert.assertTrue(dca.readPublisher().firstTypedValue().equals(name));
		Assert.assertEquals(1,dca.readContributor().size());
		Assert.assertTrue(dca.readContributor().firstTypedValue().equals(name));

		wlpAnot = new WProcessingAnnotator(doc);
		Assert.assertEquals(1,wlpAnot.readOriginalFileName().size());
		Assert.assertTrue(wlpAnot.readOriginalFileName().firstTypedValue().equals(name));


		wlpAnot.startInnerAnnotatorOn(new URI(doc.getMediaUnit().get(0).getUri()));
		Assert.assertEquals(1, wlpAnot.readNormalisedContent().size());
		Assert.assertTrue(wlpAnot.readNormalisedContent().firstTypedValue().toString().equals(doc.getMediaUnit().get(1).getUri()));
		Assert.assertEquals(1, wlpAnot.readCleansingOf().size());
		Assert.assertTrue(wlpAnot.readCleansingOf().firstTypedValue().toString().equals(doc.getMediaUnit().get(1).getUri()));

	}
}
