/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.engine.camel.utils.WebLabUtils;

public class ContentManagerTest {

	@Test
	public void testContentManagement() throws Exception{
		CamelContext context = new DefaultCamelContext();

		context.addRoutes(new RouteBuilder() {
			@Override
			public void configure() throws Exception {
				from("direct:create").to("weblab:content:create");
				from("direct:read").to("weblab:content:read");
				from("direct:update").to("weblab:content:update");
				from("direct:delete").to("weblab:content:delete");

			}
		});
		context.start();

		ProducerTemplate pt = context.createProducerTemplate();
		URI uri = pt.requestBody("direct:create", "ceci est un test",URI.class);
		Assert.assertNotNull(uri);

		final String data;
		try (InputStream stream = pt.requestBody("direct:read", uri, InputStream.class)) {
			data = IOUtils.toString(stream, StandardCharsets.UTF_8);
		}

		Assert.assertNotNull(data);


		URI uri2 = pt.requestBodyAndHeader("direct:update", "ceci est un test2", WebLabUtils.WEBLAB_CONTENT_URI, uri, URI.class);

		Assert.assertNotNull(uri2);

		Assert.assertEquals(uri, uri2);

		boolean deleted =  pt.requestBody("direct:delete", uri, Boolean.class).booleanValue();

		new File(uri).delete();

		Assert.assertTrue(deleted);
	}

}