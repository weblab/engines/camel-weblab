/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel;

import java.util.List;

import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.component.mock.MockEndpoint;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.weblab.core.model.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:camel/SplitAndMergeRoutes.xml" }, name = "splitAndMerge")
public class SplitAndMergeTest {


	@Autowired
	protected CamelContext camelContext;


	@EndpointInject(uri = "mock:resultSplitAndMerge")
	protected MockEndpoint resultSplitAndMerge;


	@Test
	public void testSplitAndMergeResources() throws Exception {

		this.camelContext.createProducerTemplate().sendBody("direct:createDocument1", "New Document 1!");
		this.camelContext.createProducerTemplate().sendBody("direct:createDocument2", "New Document 2!");
		this.camelContext.createProducerTemplate().sendBody("direct:createDocument3", "New Document 3!");

		this.resultSplitAndMerge.expectedMessageCount(1);

		this.resultSplitAndMerge.assertIsSatisfied();

		final List<Exchange> exchanges = this.resultSplitAndMerge.getExchanges();
		Assert.assertEquals(1, exchanges.size());

		final Resource resource = exchanges.get(0).getIn().getBody(Resource.class);

		Assert.assertEquals(3, resource.getAnnotation().size());
	}

}
