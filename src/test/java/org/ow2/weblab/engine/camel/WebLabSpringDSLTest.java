/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.component.cxf.CxfSpringEndpoint;
import org.apache.camel.component.cxf.DataFormat;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.cxf.binding.soap.SoapFault;
import org.junit.Assert;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.engine.camel.spring.WebLabService;
import org.springframework.core.CollectionFactory;
import org.springframework.osgi.context.support.OsgiBundleXmlApplicationContext;
import org.springframework.osgi.service.exporter.support.internal.support.ServiceRegistrationDecorator;
import org.springframework.osgi.util.internal.ClassUtils;

/**
 * Test WebLabService definition
 *
 * @author asaval
 *
 */
public class WebLabSpringDSLTest {


	/**
	 * test WebLabService spring dsl definition
	 */
	@Test
	public void testWebLabServiceDefinitionBean() {
		// need OSGi context in order to define OSGi services
		final Bundle bundle = FrameworkUtil.getBundle(WebLabSpringDSLTest.class);

		ClassUtils.getParticularClass(new Class[] { CollectionFactory.class });

		// Use Spring context to load and define Camel context/endpoint/routes
		try (final OsgiBundleXmlApplicationContext obxac = new OsgiBundleXmlApplicationContext()) {
			// set the OSGi context in Spring context
			obxac.setBundleContext(bundle.getBundleContext());
			// update to support OSGi Bundle
			// obxac.refresh();

			obxac.setConfigLocations(new String[] { "classpath:camel/beanLoad.xml", "classpath:META-INF/spring/ServiceWrappers.xml" });

			obxac.refresh();

			// check if all beans are loaded
			Assert.assertEquals(33, obxac.getBeanDefinitionCount());

			// test simple syntax for supported services
			this.checkSimpleSyntax(obxac, "test", "http://localhost:8181/test/analyser", "Analyser");
			this.checkSimpleSyntax(obxac, "test-indexer", "http://localhost:8181/test/indexer", "Indexer");
			this.checkSimpleSyntax(obxac, "test-container", "http://localhost:8181/test/container", "ResourceContainer");
			this.checkSimpleSyntax(obxac, "test-trainable", "http://localhost:8181/test/trainable", "Trainable");

			// check syntax for custom cxf & osgi properties
			this.checkCustomSyntax(obxac, "testcustom", "http://localhost:8181/testcustom/analyser", "Analyser",
					// custom cxf & osgi properties
					WebLabSpringDSLTest.toMap("customcxfproperty", "dummycxf"), WebLabSpringDSLTest.toMap("customosgiproperty", "dummyosgi"));

			// check syntax for custom cxf properties only
			this.checkCustomSyntax(obxac, "testcxf", "http://localhost:8181/testcxf/analyser", "Analyser",
					// custom cxf properties
					WebLabSpringDSLTest.toMap("customcxfpropertyonly", "dummycxfonly"), null);

			// check syntax for custom osgi properties only
			this.checkCustomSyntax(obxac, "testosgi", "http://localhost:8181/testosgi/analyser", "Analyser",
					// custom osgi properties
					null, WebLabSpringDSLTest.toMap("customosgipropertyonly", "dummyosgionly"));

		}
	}


	/**
	 * test WebLabService calls in chains
	 *
	 * @throws InterruptedException
	 * @throws WebLabCheckedException
	 * @throws IOException
	 */
	@Test
	public void testWebLabServiceSimpleSyntaxChain() throws InterruptedException, WebLabCheckedException, IOException {
		// need OSGi context in order to define OSGi services
		final Bundle bundle = FrameworkUtil.getBundle(WebLabSpringDSLTest.class);

		ClassUtils.getParticularClass(new Class[] { CollectionFactory.class });

		// Use Spring context to load and define Camel context/endpoint/routes
		try (final OsgiBundleXmlApplicationContext obxac = new OsgiBundleXmlApplicationContext()) {
			// set the OSGi context in Spring context
			obxac.setBundleContext(bundle.getBundleContext());
			// update to support OSGi Bundle
			obxac.setConfigLocations(new String[] { "classpath:camel/TestErrorsOnServiceCall.xml", "classpath:META-INF/spring/ServiceWrappers.xml" });
			obxac.refresh();

			// retrieve Camel context
			final CamelContext context = obxac.getBean("testCamelContext", CamelContext.class);
			final String text = "start";
			final MockEndpoint mockSuccess = (MockEndpoint) context.getEndpoint("mock:Success");
			final MockEndpoint mockError = (MockEndpoint) context.getEndpoint("mock:Error");

			// 1. Working Service Call --> Should success
			context.createProducerTemplate().sendBody("direct:WorkingServiceCall", text);

			// Ensure mock success received the result but not the error
			int currentlyExpectedSuccess = 1;
			int currentlyExpectedError = 0;
			mockSuccess.expectedMessageCount(currentlyExpectedSuccess);
			mockSuccess.assertIsSatisfied();
			mockError.expectedMessageCount(currentlyExpectedError);
			mockError.assertIsSatisfied();

			// Validate the result of successful service
			try (InputStream result = mockSuccess.getExchanges().get(currentlyExpectedSuccess - 1).getIn().getBody(InputStream.class)) {
				final Document document = new WebLabMarshaller().unmarshal(result, Document.class);
				Assert.assertNotNull(document);
				Assert.assertEquals(text, ((Text) document.getMediaUnit().get(0)).getContent());
			}

			// 2. Unexisting Service Call --> Should fail
			context.createProducerTemplate().sendBody("direct:UnexistingServiceCall", text);

			// Ensure mock error received the result but not the success
			currentlyExpectedError++;
			mockError.expectedMessageCount(currentlyExpectedError);
			mockError.assertIsSatisfied();
			mockSuccess.expectedMessageCount(currentlyExpectedSuccess);
			mockSuccess.assertIsSatisfied();

			// Validate error result
			final Exchange errorExchange1 = mockError.getExchanges().get(currentlyExpectedError - 1);
			Assert.assertNotNull(errorExchange1.getIn().getBody());
			Assert.assertNotNull(errorExchange1.getProperties().get("CamelExceptionCaught"));
			Assert.assertNull(errorExchange1.getIn().getHeader("weblab-service-failed"));
			Assert.assertNull(errorExchange1.getIn().getHeader("weblab-service-exception"));

			// 3. Failing Service Call --> Should fail
			context.createProducerTemplate().sendBody("direct:FailingServiceCall", text);

			// Ensure mock error received the result but not the success
			currentlyExpectedError++;
			mockError.expectedMessageCount(currentlyExpectedError);
			mockError.assertIsSatisfied();
			mockSuccess.expectedMessageCount(currentlyExpectedSuccess);
			mockSuccess.assertIsSatisfied();

			// Validate error result
			final Exchange errorExchange2 = mockError.getExchanges().get(currentlyExpectedError - 1);
			Assert.assertNotNull(errorExchange2.getIn().getBody());
			Assert.assertNotNull(errorExchange2.getProperties().get("CamelExceptionCaught"));
			Assert.assertEquals(SoapFault.class, errorExchange2.getProperties().get("CamelExceptionCaught").getClass());

			// 4. Working Service Call --> Should success
			context.createProducerTemplate().sendBody("direct:FailingButSafeServiceCall", text);

			// Ensure mock success received the result but not the error
			currentlyExpectedSuccess++;
			mockError.expectedMessageCount(currentlyExpectedError);
			mockError.assertIsSatisfied();
			mockSuccess.expectedMessageCount(currentlyExpectedSuccess);
			mockSuccess.assertIsSatisfied();


			// Validate the result of successful service
			try (InputStream result = mockSuccess.getExchanges().get(currentlyExpectedSuccess - 1).getIn().getBody(InputStream.class)) {
				final Document document = new WebLabMarshaller().unmarshal(result, Document.class);
				Assert.assertNotNull(document);
				Assert.assertEquals(text, ((Text) document.getMediaUnit().get(0)).getContent());
				String serviceFailed = mockSuccess.getExchanges().get(currentlyExpectedSuccess - 1).getIn().getHeader("weblab-service-failed", String.class);
				Assert.assertTrue(serviceFailed.indexOf(',') > 0 && serviceFailed.indexOf(',') == serviceFailed.lastIndexOf(','));
				String serviceExcep = mockSuccess.getExchanges().get(currentlyExpectedSuccess - 1).getIn().getHeader("weblab-service-exception", String.class);
				Assert.assertTrue(serviceExcep.indexOf(',') > 0 && serviceExcep.indexOf(',') == serviceExcep.lastIndexOf(','));
			}
		}

	}


	private void checkSimpleSyntax(final OsgiBundleXmlApplicationContext obxac, final String name, final String address, final String type) {
		this.checkCustomSyntax(obxac, name, address, type, null, null);
	}


	private void checkCustomSyntax(final OsgiBundleXmlApplicationContext obxac, final String name, final String address, final String type, final Map<String, Object> cxfProperties,
			final Map<String, Object> osgiProperties) {
		// check simple syntax
		final WebLabService wls = obxac.getBean(name, WebLabService.class);
		Assert.assertNotNull(wls);
		Assert.assertEquals(address, wls.getAddress());
		Assert.assertEquals(name, wls.getId());
		Assert.assertNotNull(wls.getCxfEndpointName());

		// make sure cxf endpoint has been injected
		final CxfSpringEndpoint cse = obxac.getBean(wls.getCxfEndpointName(), CxfSpringEndpoint.class);
		Assert.assertNotNull(cse);
		// check default properties
		Assert.assertTrue(cse.getAllowStreaming().booleanValue());
		Assert.assertEquals(DataFormat.PAYLOAD, cse.getDataFormat());

		// check cxf custom properties
		if (cxfProperties != null) {
			for (final Entry<String, Object> entry : cxfProperties.entrySet()) {
				final Object value = cse.getProperties().get(entry.getKey());
				Assert.assertNotNull("Value for key " + entry.getKey() + " should be : " + entry.getValue(), value);
				Assert.assertEquals(entry.getValue(), value);
			}
		}

		// make sure osgi service has been injected
		final ServiceRegistrationDecorator osfb = obxac.getBean(wls.getOsgiServiceName(), ServiceRegistrationDecorator.class);
		Assert.assertNotNull(osfb);
		// check default properties
		Assert.assertEquals(name, osfb.getReference().getProperty("name"));
		Assert.assertEquals(type, osfb.getReference().getProperty("type"));

		// check osgi custom properties
		if (osgiProperties != null) {
			for (final Entry<String, Object> entry : osgiProperties.entrySet()) {
				final Object value = osfb.getReference().getProperty(entry.getKey());
				Assert.assertNotNull("Value for key " + entry.getKey() + " should be : " + entry.getValue(), value);
				Assert.assertEquals(entry.getValue(), value);
			}
		}
	}


	private static Map<String, Object> toMap(final String... strings) {
		final HashMap<String, Object> map = new HashMap<>();
		for (int i = 0; i < strings.length; i += 2) {
			map.put(strings[i], strings[i + 1]);
		}
		return map;
	}

}