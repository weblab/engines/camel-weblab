/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.Video;
import org.ow2.weblab.core.model.extended.GoodOriginalResource;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:camel/ResourcesRoutes.xml" }, name = "Resources")
public class WeblabComponentResourceCreationTest {


	@Autowired
	protected CamelContext camelContext;


	@EndpointInject(uri = "mock:resultDocument")
	protected MockEndpoint mockDocument;


	@EndpointInject(uri = "mock:resultText")
	protected MockEndpoint mockText;


	@EndpointInject(uri = "mock:resultImage")
	protected MockEndpoint mockImage;


	@EndpointInject(uri = "mock:resultVideo")
	protected MockEndpoint mockVideo;


	@EndpointInject(uri = "mock:resultAudio")
	protected MockEndpoint mockAudio;


	@EndpointInject(uri = "mock:resultResource")
	protected MockEndpoint mockResource;


	@EndpointInject(uri = "mock:resultDocumentWithoutContentDuplication")
	protected MockEndpoint mockDocumentWithoutContentDuplication;


	@EndpointInject(uri = "mock:resultDocumentWithContentDuplication")
	protected MockEndpoint mockDocumentWithContentDuplication;


	@EndpointInject(uri = "mock:resultAnnotatedText")
	protected MockEndpoint mockAnnotatedText;


	@EndpointInject(uri = "mock:resultWrappedText")
	protected MockEndpoint mockWrappedText;


	@EndpointInject(uri = "mock:resultBadOriginalResource")
	protected MockEndpoint mockBOResource;


	@EndpointInject(uri = "mock:errorResourceCreation")
	protected MockEndpoint mockErrorResourceCreation;


	@EndpointInject(uri = "mock:resultGoodOriginalResource")
	protected MockEndpoint mockGOResource;


	@Test
	public void testWeblabResourceCreation() throws Exception {
		// test Document creation
		this.camelContext.createProducerTemplate().sendBody("direct:createDocument", "New Document !");

		this.mockDocument.expectedMessageCount(1);
		this.mockDocument.assertIsSatisfied();
		Object body = this.mockDocument.getReceivedExchanges().get(0).getIn().getBody();
		Assert.assertTrue(body instanceof Document);

		// test Text creation
		this.camelContext.createProducerTemplate().sendBody("direct:createText", "New Text !");

		this.mockText.expectedMessageCount(1);
		this.mockText.assertIsSatisfied();
		body = this.mockText.getReceivedExchanges().get(0).getIn().getBody();
		Assert.assertTrue(body instanceof Text);

		// test Image creation
		this.camelContext.createProducerTemplate().sendBody("direct:createImage", "New Image !");

		this.mockImage.expectedMessageCount(1);
		this.mockImage.assertIsSatisfied();
		body = this.mockImage.getReceivedExchanges().get(0).getIn().getBody();
		Assert.assertTrue(body instanceof Image);

		// test Video creation
		this.camelContext.createProducerTemplate().sendBody("direct:createVideo", "New Video !");

		this.mockVideo.expectedMessageCount(1);
		this.mockVideo.assertIsSatisfied();
		body = this.mockVideo.getReceivedExchanges().get(0).getIn().getBody();
		Assert.assertTrue(body instanceof Video);

		// test Audio creation
		this.camelContext.createProducerTemplate().sendBody("direct:createAudio", "New Audio !");

		this.mockAudio.expectedMessageCount(1);
		this.mockAudio.assertIsSatisfied();
		body = this.mockAudio.getReceivedExchanges().get(0).getIn().getBody();
		Assert.assertTrue(body instanceof Audio);

		// test Resource creation
		this.camelContext.createProducerTemplate().sendBody("direct:createResource", "New Resource !");

		this.mockResource.expectedMessageCount(1);
		this.mockResource.assertIsSatisfied();
		body = this.mockResource.getReceivedExchanges().get(0).getIn().getBody();
		Assert.assertTrue(body instanceof Resource);


		// test Document creation without content duplication
		File directory = new File("target/createDocumentWithoutContentDuplication");
		if (directory.exists()) {
			FileUtils.cleanDirectory(directory);
		}
		File localFile = new File(directory, +System.nanoTime() + ".wav");
		File originalInput = new File("src/test/resources/audios/file3.wav");
		FileUtils.copyFile(originalInput, localFile);
		Assert.assertTrue(FileUtils.contentEquals(originalInput, localFile));
		Exchange ex = this.camelContext.createConsumerTemplate().receive("file:" + directory.getPath() + "?noop=true", 3000);
		Assert.assertNotNull(ex);
		this.camelContext.createProducerTemplate().send("direct:createDocumentWithoutContentDuplication", ex);
		this.mockDocumentWithoutContentDuplication.expectedMessageCount(1);
		this.mockDocumentWithoutContentDuplication.assertIsSatisfied();
		body = this.mockDocumentWithoutContentDuplication.getReceivedExchanges().get(0).getIn().getBody();
		Assert.assertNotNull(body);
		Assert.assertTrue(body instanceof Document);
		File nativeContent = ContentManager.getInstance().readLocalExistingFile(new WProcessingAnnotator((Document) body).readNativeContent().firstTypedValue(), null, null);
		Assert.assertNotNull(nativeContent);
		Assert.assertEquals(nativeContent.getAbsolutePath(), localFile.getAbsolutePath());
		Assert.assertTrue(FileUtils.contentEquals(nativeContent, originalInput));



		// test Document creation with content duplication
		directory = new File("target/createDocumentWithContentDuplication");
		if (directory.exists()) {
			FileUtils.cleanDirectory(directory);
		}
		localFile = new File(directory, System.nanoTime() + ".wav");
		FileUtils.copyFile(originalInput, localFile);
		Assert.assertTrue(FileUtils.contentEquals(originalInput, localFile));
		ex = this.camelContext.createConsumerTemplate().receive("file:" + directory.getPath() + "?noop=true", 3000);
		Assert.assertNotNull(ex);
		this.camelContext.createProducerTemplate().send("direct:createDocumentWithContentDuplication", ex);
		this.mockDocumentWithContentDuplication.expectedMessageCount(1);
		this.mockDocumentWithContentDuplication.assertIsSatisfied();
		body = this.mockDocumentWithContentDuplication.getReceivedExchanges().get(0).getIn().getBody();
		Assert.assertNotNull(body);
		Assert.assertTrue(body instanceof Document);
		nativeContent = ContentManager.getInstance().readLocalExistingFile(new WProcessingAnnotator((Document) body).readNativeContent().firstTypedValue(), null, null);
		Assert.assertNotNull(nativeContent);
		Assert.assertNotEquals(nativeContent.getAbsolutePath(), localFile.getAbsolutePath());
		Assert.assertTrue(FileUtils.contentEquals(nativeContent, originalInput));



		// Wrapped resource creation
		this.camelContext.createProducerTemplate().sendBody("direct:createWrappedText", "New Resource !");

		this.mockWrappedText.expectedMessageCount(1);
		this.mockWrappedText.assertIsSatisfied();
		body = this.mockWrappedText.getReceivedExchanges().get(0).getIn().getBody();
		Assert.assertTrue(body instanceof Document);
		Assert.assertTrue(((Document) body).getMediaUnit().get(0) instanceof Text);


		// Annotated resource creation
		final Map<String, Object> headers = new HashMap<>();
		headers.put("weblab:dc:title@lang=en", "New title!");
		headers.put("weblab:dc:title@lang=fr", "Nouveau titre!");
		headers.put("weblab:dc:date", new Date());
		headers.put("weblab:wlp:hasOriginalFileSize", Long.valueOf(123243L));

		this.camelContext.createProducerTemplate().sendBodyAndHeaders("direct:createAnnotatedText", "New Resource !", headers);

		this.mockAnnotatedText.expectedMessageCount(1);
		this.mockAnnotatedText.assertIsSatisfied();
		body = this.mockAnnotatedText.getReceivedExchanges().get(0).getIn().getBody();
		Assert.assertTrue(body instanceof Text);
		final DublinCoreAnnotator dca = new DublinCoreAnnotator((Resource) body);
		final WProcessingAnnotator wpa = new WProcessingAnnotator((Resource) body);

		Assert.assertTrue(dca.readDate().firstTypedValue().getClass().equals(Date.class));
		Assert.assertTrue(dca.readDate().firstTypedValue().equals(headers.get("weblab:dc:date")));

		Assert.assertTrue(dca.readTitle().firstTypedValue("en").equals(headers.get("weblab:dc:title@lang=en")));
		Assert.assertTrue(dca.readTitle().firstTypedValue("fr").equals(headers.get("weblab:dc:title@lang=fr")));

		Assert.assertTrue(wpa.readOriginalFileSize().firstTypedValue().getClass().equals(Long.class));
		Assert.assertTrue(wpa.readOriginalFileSize().firstTypedValue().equals(headers.get("weblab:wlp:hasOriginalFileSize")));

		Assert.assertTrue(((Text) body).getContent().equals("New Resource !"));

		// test bad Resource creation
		this.camelContext.createProducerTemplate().sendBody("direct:createBadOriginalResource", "New Resource !");
		// should fail
		this.mockBOResource.expectedMessageCount(0);
		this.mockBOResource.assertIsSatisfied(5000);

		this.mockErrorResourceCreation.expectedMessageCount(1);
		this.mockErrorResourceCreation.assertIsSatisfied();

		// test good extended Resource creation
		this.camelContext.createProducerTemplate().sendBody("direct:createGoodOriginalResource", "New GoodOriginal Resource !");

		this.mockGOResource.expectedMessageCount(1);
		this.mockGOResource.assertIsSatisfied();
		body = this.mockGOResource.getReceivedExchanges().get(0).getIn().getBody();
		Assert.assertTrue(body instanceof GoodOriginalResource);
	}

}
