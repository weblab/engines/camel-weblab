/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel;

import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Text;
import org.springframework.osgi.context.support.OsgiBundleXmlApplicationContext;

public class OSGiSpringCamelTest {


	@Test
	public void testWebServiceCallFromOSGiRegistry() throws Exception {
		FrameworkUtil.loadedOnce();
		// init OSGi Framework
		// TODO: find a better way to load OSGi context into Camel & Spring
		Bundle bundle = FrameworkUtil.getBundle(null);

		// Use Spring context to load and define Camel context/endpoint/routes

		try (final OsgiBundleXmlApplicationContext obxac = new OsgiBundleXmlApplicationContext()) {
			// set the OSGi context in Spring context
			obxac.setBundleContext(bundle.getBundleContext());
			// update to support OSGi Bundle

			obxac.setConfigLocations(new String[] { "classpath:META-INF/spring/ServiceWrappers.xml", "classpath:camel/TestChainWithOSGi.xml" });

			obxac.refresh();
			// retrieve Camel context
			CamelContext context = obxac.getBean("testCamelContext", CamelContext.class);

			// retrieve mocks up
			MockEndpoint meDirect = (MockEndpoint) context.getEndpoint("mock:resultDirectCall");
			MockEndpoint meOSGi = (MockEndpoint) context.getEndpoint("mock:resultOSGiCall");
			MockEndpoint trainATR_OSGi = (MockEndpoint) context.getEndpoint("mock:result-trainableCall-addTrainResource");
			MockEndpoint trainT_OSGi = (MockEndpoint) context.getEndpoint("mock:result-trainableCall-train");
			MockEndpoint trainRTM_OSGi = (MockEndpoint) context.getEndpoint("mock:result-trainableCall-resetTrainedModel");
			MockEndpoint trainnoOP_OSGi = (MockEndpoint) context.getEndpoint("mock:result-trainableCall-noOP");
			MockEndpoint trainMiss_OSGi = (MockEndpoint) context.getEndpoint("mock:direct:trainableCall-IPutA(Mis)SpellOnYou");

			context.start();

			// start test route
			ProducerTemplate producer = context.createProducerTemplate();

			String directCall = "Direct call text";
			String osgiCall = "OSGi call text";

			producer.sendBody("direct:directCall", directCall);

			producer.sendBody("direct:osgiCall", osgiCall);

			// test TRAINABLE routes.
			producer.sendBody("direct:trainableCall-addTrainResource", "");
			producer.sendBody("direct:trainableCall-train", "");
			producer.sendBody("direct:trainableCall-resetTrainedModel", "");
			try {
				producer.sendBody("direct:trainableCall-noOP-dontDoThat", "");
				Assert.fail();
			} catch (Exception e) {
				LogFactory.getLog(OSGiSpringCamelTest.class).debug("SOAP call to a SOAP endpoint with differents operations available without any operation specified correctly throw exception");
			}
			try {
				producer.sendBody("direct:trainableCall-IPutA(Mis)SpellOnYou", "");
				Assert.fail();
			} catch (Exception e) {
				LogFactory.getLog(OSGiSpringCamelTest.class).debug("SOAP call to a SOAP endpoint with differents operations available with a wrong operation specification correctly throw exception");
			}


			meDirect.expectedMessageCount(1);
			meDirect.assertIsSatisfied();
			InputStream result = null;
			Object body = meDirect.getExchanges().get(0).getIn().getBody();
			if (body instanceof InputStream) {
				result = (InputStream) body;
			} else {
				result = IOUtils.toInputStream(body.toString(), StandardCharsets.UTF_8);
			}

			Text text = new WebLabMarshaller().unmarshal(result, Text.class);
			Assert.assertNotNull(text);
			Assert.assertEquals(directCall, text.getContent());
			meOSGi.expectedMessageCount(1);
			meOSGi.assertIsSatisfied();

			body = meOSGi.getExchanges().get(0).getIn().getBody();
			if (body instanceof InputStream) {
				result = (InputStream) body;
			} else {
				result = IOUtils.toInputStream(body.toString(), StandardCharsets.UTF_8);
			}

			Document document = new WebLabMarshaller().unmarshal(result, Document.class);
			Assert.assertNotNull(document);
			Assert.assertEquals(osgiCall, ((Text) document.getMediaUnit().get(0)).getContent());
			// Test for trainable OSGi_mocks

			trainATR_OSGi.expectedMessageCount(1);
			trainATR_OSGi.assertIsSatisfied();

			trainT_OSGi.expectedMessageCount(1);
			trainT_OSGi.assertIsSatisfied();

			trainRTM_OSGi.expectedMessageCount(1);
			trainRTM_OSGi.assertIsSatisfied();

			trainnoOP_OSGi.expectedMessageCount(0);
			trainnoOP_OSGi.assertIsSatisfied();

			trainMiss_OSGi.expectedMessageCount(0);
			trainMiss_OSGi.assertIsSatisfied();


			// test RESOURCE CONTAINER routes.

			// save
			producer.sendBody("direct:resourceContainerSaveCall", osgiCall);

			meOSGi.expectedMessageCount(2);
			meOSGi.assertIsSatisfied();

			body = meOSGi.getExchanges().get(1).getIn().getBody();


			producer.sendBody("direct:resourceContainerLoadCall", body);

			meOSGi.expectedMessageCount(3);
			meOSGi.assertIsSatisfied();

			body = meOSGi.getExchanges().get(2).getIn().getBody();
			if (body instanceof InputStream) {
				result = (InputStream) body;
			} else {
				result = IOUtils.toInputStream(body.toString(), StandardCharsets.UTF_8);
			}


			document = new WebLabMarshaller().unmarshal(result, Document.class);
			Assert.assertNotNull(document);
			Assert.assertEquals(osgiCall, ((Text) document.getMediaUnit().get(0)).getContent());

			// test CLEANABLE routes.

			// clean
			List<String> uris = new ArrayList<>();
			uris.add("uri1");
			uris.add("uri2");

			producer.sendBody("direct:cleanableCleanCall", uris);

			meOSGi.expectedMessageCount(4);
			meOSGi.assertIsSatisfied();
			Assert.assertNull(meOSGi.getExchanges().get(3).getIn().getBody());

			// clean (single URI)
			producer.sendBody("direct:cleanableCleanCall", "weblab://myResourceURI");

			meOSGi.expectedMessageCount(5);
			meOSGi.assertIsSatisfied();
			Assert.assertNull(meOSGi.getExchanges().get(4).getIn().getBody());


			// clean (single URI)
			producer.sendBody("direct:cleanableCleanCall", new URI("weblab://myResourceURI"));

			meOSGi.expectedMessageCount(6);
			meOSGi.assertIsSatisfied();
			Assert.assertNull(meOSGi.getExchanges().get(5).getIn().getBody());

		}
	}
}
