/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel;

import java.net.URI;

import javax.xml.transform.TransformerException;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.engine.camel.dataformat.URIConverter;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.w3c.dom.UserDataHandler;

public class URIConverterTest {


	@Test
	public void testURINullformat() {
		final Node nullNode = null;
		final NodeList nullNodeList = null;
		final String nullString = null;

		try {
			final URI uri = URIConverter.convertTo(nullNode);
			Assert.fail("URIConverter should fail on null org.w3c.dom.Node value, result was : " + uri);
		} catch (final TransformerException e) {
			// expected
		}
		try {
			final URI uri = URIConverter.convertTo(nullNodeList);
			Assert.fail("URIConverter should fail on null org.w3c.dom.NodeList value, result was : " + uri);
		} catch (final TransformerException e) {
			// expected
		}
		try {
			final URI uri = URIConverter.convertTo(nullString);
			Assert.fail("URIConverter should fail on null String value, result was : " + uri);
		} catch (final TransformerException e) {
			// expected
		}
	}


	@Test
	public void testURIformat() {
		final Node node = new MockNode();
		final MockNodeList nodeList = new MockNodeList();
		final String good_uri = "weblab://camel#rocks";
		final String bad_uri = "#what#means#mot-dièse?";

		URI uri = null;
		try {
			// test empty Node
			uri = URIConverter.convertTo(node);
			Assert.fail("URIConverter should fail on empty Node, result was : " + uri);
		} catch (final TransformerException te) {
			// excepted
		}

		try {
			// test empty NodeList
			uri = URIConverter.convertTo(nodeList);
			Assert.fail("URIConverter should fail on empty NodeList, result was : " + uri);
		} catch (final TransformerException te) {
			// expected
		}

		try {
			// test NodeList with Empty Node
			nodeList.setNode(node);
			uri = URIConverter.convertTo(nodeList);
			Assert.fail("URIConverter should fail on NodeList containing empty Node, result was : " + uri);
		} catch (final TransformerException te) {
			// expected
		}

		try {
			uri = URIConverter.convertTo(good_uri);
		} catch (final TransformerException e) {
			Assert.fail("URIConverter failed to convert " + good_uri + " to URI : " + e.getMessage());
		}

		try {
			uri = URIConverter.convertTo(good_uri);
		} catch (final TransformerException e) {
			Assert.fail("URIConverter failed to convert " + good_uri + " to URI : " + e.getMessage());
		}

		try {
			uri = URIConverter.convertTo(bad_uri);
			Assert.fail("URIConverter should fail on non valid uri such as " + bad_uri);
		} catch (final TransformerException e) {
			// expected
		}


		// set bad uri into node
		node.setTextContent(bad_uri);

		try {
			uri = URIConverter.convertTo(node);
			Assert.fail("URIConverter should fail on non valid uri such as " + bad_uri);
		} catch (final TransformerException e) {
			// expected
		}

		try {
			uri = URIConverter.convertTo(nodeList);
			Assert.fail("URIConverter should fail on non valid uri such as " + bad_uri);
		} catch (final TransformerException e) {
			// expected
		}

		// set good uri into node
		node.setTextContent(good_uri);

		try {
			uri = URIConverter.convertTo(node);
		} catch (final TransformerException e) {
			Assert.fail("URIConverter failed to convert node to URI " + node.getTextContent() + " : " + e.getMessage());
		}

		try {
			uri = URIConverter.convertTo(nodeList);
		} catch (final TransformerException e) {
			Assert.fail("URIConverter failed to convert nodeList to URI: " + e.getMessage());
		}
	}


	/**
	 * Node mockup
	 *
	 * @author asaval
	 *
	 */
	private class MockNode implements Text {


		private short type = 0;


		private String text;


		public MockNode() {
			// Nothing to do
		}


		@Override
		public Node appendChild(final Node newChild) throws DOMException {
			return null;
		}


		@Override
		public Node cloneNode(final boolean deep) {
			return this;
		}


		@Override
		public short compareDocumentPosition(final Node other) throws DOMException {
			return 0;
		}


		@Override
		public NamedNodeMap getAttributes() {
			return null;
		}


		@Override
		public String getBaseURI() {
			return null;
		}


		@Override
		public NodeList getChildNodes() {
			return null;
		}


		@Override
		public Object getFeature(final String feature, final String version) {
			return null;
		}


		@Override
		public Node getFirstChild() {
			return null;
		}


		@Override
		public Node getLastChild() {
			return null;
		}


		@Override
		public String getLocalName() {
			return this.text;
		}


		@Override
		public String getNamespaceURI() {
			return null;
		}


		@Override
		public Node getNextSibling() {
			return null;
		}


		@Override
		public String getNodeName() {
			return null;
		}


		@Override
		public short getNodeType() {
			return this.type;
		}


		@Override
		public String getNodeValue() throws DOMException {
			return this.text;
		}


		@Override
		public Document getOwnerDocument() {
			return null;
		}


		@Override
		public Node getParentNode() {
			return null;
		}


		@Override
		public String getPrefix() {
			return null;
		}


		@Override
		public Node getPreviousSibling() {
			return null;
		}


		@Override
		public String getTextContent() throws DOMException {
			return this.text;
		}


		@Override
		public Object getUserData(final String key) {
			return null;
		}


		@Override
		public boolean hasAttributes() {
			return false;
		}


		@Override
		public boolean hasChildNodes() {
			return false;
		}


		@Override
		public Node insertBefore(final Node newChild, final Node refChild) throws DOMException {
			return null;
		}


		@Override
		public boolean isDefaultNamespace(final String namespaceURI) {
			return false;
		}


		@Override
		public boolean isEqualNode(final Node arg) {
			return false;
		}


		@Override
		public boolean isSameNode(final Node other) {
			return false;
		}


		@Override
		public boolean isSupported(final String feature, final String version) {
			return false;
		}


		@Override
		public String lookupNamespaceURI(final String prefix) {
			return null;
		}


		@Override
		public String lookupPrefix(final String namespaceURI) {
			return null;
		}


		@Override
		public void normalize() {
			// Nothing to do
		}


		@Override
		public Node removeChild(final Node oldChild) throws DOMException {
			return null;
		}


		@Override
		public Node replaceChild(final Node newChild, final Node oldChild) throws DOMException {
			return null;
		}


		@Override
		public void setNodeValue(final String nodeValue) throws DOMException {
			// Nothing to do
		}


		@Override
		public void setPrefix(final String prefix) throws DOMException {
			// Nothing to do
		}


		@Override
		public void setTextContent(final String textContent) throws DOMException {
			this.text = textContent;
			if (textContent == null) {
				this.type = 0;
			} else {
				this.type = 3;
			}
		}


		@Override
		public Object setUserData(final String key, final Object data, final UserDataHandler handler) {
			return null;
		}


		@Override
		public void appendData(final String arg) throws DOMException {
			// Nothing to do
		}


		@Override
		public void deleteData(final int offset, final int count) throws DOMException {
			// Nothing to do
		}


		@Override
		public String getData() throws DOMException {
			return this.text;
		}


		@Override
		public int getLength() {
			return this.text.length();
		}


		@Override
		public void insertData(final int offset, final String arg) throws DOMException {
			// Nothing to do
		}


		@Override
		public void replaceData(final int offset, final int count, final String arg) throws DOMException {
			// Nothing to do
		}


		@Override
		public void setData(final String data) throws DOMException {
			// Nothing to do
		}


		@Override
		public String substringData(final int offset, final int count) throws DOMException {
			return this.text;
		}


		@Override
		public String getWholeText() {
			return this.text;
		}


		@Override
		public boolean isElementContentWhitespace() {
			return false;
		}


		@Override
		public Text replaceWholeText(final String content) throws DOMException {
			return this;
		}


		@Override
		public Text splitText(final int offset) throws DOMException {
			return this;
		}


	}


	/**
	 * NodeList mockup
	 *
	 * @author asaval
	 *
	 */
	private class MockNodeList implements NodeList {


		private Node node;


		public MockNodeList() {
			// Nothing to do
		}


		@Override
		public int getLength() {
			return this.node == null ? 0 : 1;
		}


		@Override
		public Node item(final int arg0) {
			return this.node;
		}


		public void setNode(final Node node) {
			this.node = node;
		}
	}

}
