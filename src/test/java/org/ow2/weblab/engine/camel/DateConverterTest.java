/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel;

import java.util.Date;

import javax.xml.transform.TransformerException;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.engine.camel.dataformat.DateConverter;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class DateConverterTest {


	@Test(expected = TransformerException.class)
	public void testNullToDate() throws TransformerException {
		DateConverter.objectToDate(null);
	}


	@Test(expected = TransformerException.class)
	public void testEmptyStringToDate() throws TransformerException {
		DateConverter.objectToDate("");
	}


	@Test(expected = TransformerException.class)
	public void testDateConverterToDate() throws TransformerException {
		DateConverter.objectToDate(new DateConverter());
	}


	@Test(expected = TransformerException.class)
	public void testNotADate() throws TransformerException {
		DateConverter.objectToDate("35notseconds");
	}


	@Test(expected = TransformerException.class)
	public void testNotHandledDateFormat() throws TransformerException {
		DateConverter.objectToDate("31/09/2013");
	}


	@Test(expected = TransformerException.class)
	public void testNullToISODate() throws TransformerException {
		DateConverter.stringToISO8601Date(null);
	}


	@Test(expected = TransformerException.class)
	public void testEmptyStringToISODate() throws TransformerException {
		DateConverter.stringToISO8601Date("");
	}


	@Test(expected = TransformerException.class)
	public void testNotAnISODate() throws TransformerException {
		DateConverter.stringToISO8601Date("35notseconds");
	}


	@Test(expected = TransformerException.class)
	public void testNotHandledISODateFormat() throws TransformerException {
		DateConverter.stringToISO8601Date("31/09/2013");
	}


	@Test(expected = TransformerException.class)
	public void testNullToRFCDate() throws TransformerException {
		DateConverter.stringToRFC2616Date(null);
	}


	@Test(expected = TransformerException.class)
	public void testEmptyStringToRFCDate() throws TransformerException {
		DateConverter.stringToRFC2616Date("");
	}


	@Test(expected = TransformerException.class)
	public void testNotAnRFCDate() throws TransformerException {
		DateConverter.stringToRFC2616Date("35notseconds");
	}


	@Test(expected = TransformerException.class)
	public void testNotHandledRFCDateFormat() throws TransformerException {
		DateConverter.stringToRFC2616Date("31/09/2013");
	}


	@Test(expected = TransformerException.class)
	public void testTooLongRFCDate() throws Exception {
		DateConverter.stringToRFC2616Date("Wed, 28 Aug 2013 00:00:00 GMT toto");
	}


	@Test(expected = TransformerException.class)
	public void testTooLongISODate() throws Exception {
		DateConverter.stringToISO8601Date("2013-08-28T02:00:00+0200 toto");
	}


	@Test(expected = TransformerException.class)
	public void testTooLongDate() throws Exception {
		DateConverter.objectToDate("2013-08-28T02:00:00+0200 toto");
	}


	@Test(expected = TransformerException.class)
	public void testNullToTimestamp() throws TransformerException {
		DateConverter.dateToTimestamp(null);
	}


	@Test(expected = TransformerException.class)
	public void testNullToRFC() throws TransformerException {
		DateConverter.dateToRFC2616Date(null);
	}


	@Test(expected = TransformerException.class)
	public void testNullToISO() throws TransformerException {
		DateConverter.dateToISO8601Date(null);
	}


	@Test
	public void dateToDate() throws Exception {
		Date date = new Date();
		Assert.assertEquals(date, DateConverter.objectToDate(date));
		Assert.assertEquals(date.getTime(), DateConverter.dateToISO8601Date(date).getTime());
		Assert.assertEquals(date.getTime(), DateConverter.dateToRFC2616Date(date).getTime());
		Assert.assertEquals(date, DateConverter.dateToTimestamp(date));

	}


	@Test
	public void testDateFormat() throws TransformerException {
		final long milliseconds = 1377648000000l; // The 2013-08-28 00:00:00 GMT
		final Date expectedDate = new Date(milliseconds);

		Assert.assertEquals(expectedDate, DateConverter.objectToDate("2013-08-28"));// yyyy-mm-dd
		Assert.assertEquals(expectedDate, DateConverter.objectToDate("2013-240"));// yyyy-DDD
		Assert.assertEquals(expectedDate, DateConverter.objectToDate("2013-08-28T00:00:00"));// full
		Assert.assertEquals(expectedDate, DateConverter.objectToDate("2013-08-28T00:00:00Z"));// full
		Assert.assertEquals(expectedDate, DateConverter.objectToDate("2013-08-28T02:00:00+0200"));// full plus time zone
		Assert.assertEquals(expectedDate, DateConverter.objectToDate("2013-08-28T02:00:00+02:00"));// full plus time zone

		Assert.assertEquals(expectedDate.getTime(), DateConverter.objectToDate(Long.valueOf(milliseconds)).getTime());

		Assert.assertEquals(expectedDate.getTime(), DateConverter.stringToRFC2616Date("Wed, 28 Aug 2013 00:00:00 GMT").getTime());
		Assert.assertEquals(expectedDate.getTime(), DateConverter.stringToRFC2616Date("Wednesday, 28-Aug-13 00:00:00 GMT").getTime());
		Assert.assertEquals(expectedDate.getTime(), DateConverter.stringToRFC2616Date("Wed Aug 28 00:00:00 2013").getTime());

		Assert.assertEquals(expectedDate.getTime(), DateConverter.stringToISO8601Date("2013-08-28").getTime());// yyyy-mm-dd
		Assert.assertEquals(expectedDate.getTime(), DateConverter.stringToISO8601Date("2013-240").getTime());// yyyy-DDD
		Assert.assertEquals(expectedDate.getTime(), DateConverter.stringToISO8601Date("2013-08-28T00:00:00").getTime());// full
		Assert.assertEquals(expectedDate.getTime(), DateConverter.stringToISO8601Date("2013-08-28T00:00:00Z").getTime());// full
		Assert.assertEquals(expectedDate.getTime(), DateConverter.stringToISO8601Date("2013-08-28T02:00:00+0200").getTime());// full plus time zone
		Assert.assertEquals(expectedDate.getTime(), DateConverter.stringToISO8601Date("2013-08-28T02:00:00+02:00").getTime());// full plus time zone
	}


	@Test
	public void testdate() throws TransformerException {
		String date = "2014-04-25T17:37:04.643+0200";
		DateConverter.objectToDate(date);
	}


	@Test
	public void dateTestXML() throws Exception {
		try (final FileSystemXmlApplicationContext bean = new FileSystemXmlApplicationContext("src/test/resources/camel/datetest.xml")) {
			final CamelContext context = bean.getBean("testdate", CamelContext.class);


			final RouteBuilder routeBuilder = new RouteBuilder() {


				@Override
				public void configure() throws Exception {
					this.from("direct:check-results").process(new Processor() {


						@Override
						public void process(final Exchange exchange) throws Exception {
							// check dates
							final Date date = exchange.getIn().getHeader("date", Date.class);
							final Date date1 = exchange.getIn().getHeader("date1", Date.class);
							final Date date2 = exchange.getIn().getHeader("date2", Date.class);
							final Date date3 = exchange.getIn().getHeader("date3", Date.class);
							final Date date4 = exchange.getIn().getHeader("date4", Date.class);
							final Date date5 = exchange.getIn().getHeader("date5", Date.class);
							final Date date6 = exchange.getIn().getHeader("date6", Date.class);

							Assert.assertEquals(1377648000000l, date.getTime());
							Assert.assertEquals(1377640800000l, date1.getTime());
							Assert.assertNull(date2);
							Assert.assertEquals(1386028800000l, date3.getTime());
							Assert.assertNull(date4);
							Assert.assertEquals(1386028800000l, date5.getTime());
							Assert.assertNotNull(date6);
							Assert.assertEquals(1398440224643l, date6.getTime());
						}
					});

				}

			};

			routeBuilder.addRoutesToCamelContext(context);

			context.start();

			final ProducerTemplate pt = context.createProducerTemplate();

			// launch tests
			pt.sendBody("direct:test-date-all", "dummy");
		}
	}
}