/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel;

import java.io.File;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.component.mock.MockEndpoint;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.purl.dc.terms.DCTermsAnnotator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:camel/WarcRoutes.xml" }, name = "warcProcess")
public class WeblabComponentTest {


	private static final int EXPECTED_NB_RECORD = 68;


	private static final Collection<String> ALLOWED_FORMAT = Arrays.asList("text/html", "text/plain");


	@Autowired
	protected CamelContext camelContext;


	@EndpointInject(uri = "mock:resultWarcs")
	protected MockEndpoint mockWarcsToResources;


	@Test
	public void testWarcsToResources() throws Exception {
		// test warc process to Text resources

		Assert.assertNotNull(this.camelContext.hasEndpoint("file:src/test/resources/warcs?noop=true"));

		this.mockWarcsToResources.expectedMessageCount(EXPECTED_NB_RECORD);
		this.mockWarcsToResources.setResultWaitTime(60000);
		this.mockWarcsToResources.assertIsSatisfied();

		final List<Exchange> exchanges = this.mockWarcsToResources.getExchanges();

		Assert.assertEquals(EXPECTED_NB_RECORD, exchanges.size());
		int noModified = 0;
		File target = new File("target/warcRes");
		target.mkdirs();
		for (final Exchange exchange : exchanges) {
			Assert.assertTrue(exchange.getIn().getBody() instanceof String);
			final String docStr = exchange.getIn().getBody(String.class);
			final Document doc = new WebLabMarshaller(true).unmarshal(new StringReader(docStr), Document.class);
			Assert.assertNotNull(doc.getUri());

			final WProcessingAnnotator wpa = new WProcessingAnnotator(doc);
			Assert.assertTrue(wpa.readOriginalFileSize().hasValue());
			Assert.assertTrue(wpa.readOriginalFileSize().firstTypedValue().longValue() > 0);
			Assert.assertTrue(wpa.readGatheringDate().hasValue());
			Assert.assertTrue(wpa.readNativeContent().hasValue());

			final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
			Assert.assertTrue(dca.readSource().hasValue());
			Assert.assertTrue(dca.readFormat().hasValue());
			Assert.assertTrue(WeblabComponentTest.ALLOWED_FORMAT.contains(dca.readFormat().firstTypedValue()));

			final DCTermsAnnotator dcta = new DCTermsAnnotator(doc);
			if (dcta.readModified().hasValue()) {
				Date modified = dcta.readModified().firstTypedValue();
				Date gathered = wpa.readGatheringDate().firstTypedValue();
				Assert.assertTrue(modified.before(gathered) || modified.equals(gathered));
			} else {
				noModified++;
			}
			new WebLabMarshaller().marshalResource(doc, new File(target, doc.getUri().hashCode() + ".xml"));
		}
		Assert.assertTrue(noModified < EXPECTED_NB_RECORD);
	}

}
