<?xml version="1.0" encoding="UTF-8"?>
<spring:beans xmlns:spring="http://www.springframework.org/schema/beans" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://camel.apache.org/schema/spring" xmlns:cxf="http://camel.apache.org/schema/cxf"
	xsi:schemaLocation="http://www.springframework.org/schema/beans  http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
         http://camel.apache.org/schema/spring http://camel.apache.org/schema/spring/camel-spring.xsd
         http://camel.apache.org/schema/dataFormats http://camel.apache.org/schema/spring/camel-spring.xsd
         http://cxf.apache.org/jaxws http://cxf.apache.org/schemas/jaxws.xsd
         http://camel.apache.org/schema/cxf http://camel.apache.org/schema/cxf/camel-cxf.xsd">


	<!--======================================================= -->
	<!--========== BEANS =========== -->
	<!--======================================================= -->


	<!-- To prepare WARC splitter -->
	<spring:bean id="warcSplitter" class="org.ow2.weblab.engine.camel.WarcSplitter">
		<spring:property name="copyWarcInfoHeaders" value="true" />
		<spring:property name="copyMetadataHeaders" value="true" />
		<spring:property name="forwardedRecords">
			<spring:list>
				<spring:value>response</spring:value>
			</spring:list>
		</spring:property>
	</spring:bean>


	<camelContext id="warcProcess" xmlns="http://camel.apache.org/schema/spring">

		<dataFormats>
			<jaxb id="myJaxb" prettyPrint="true" contextPath="org.ow2.weblab.core.model" partClass="org.ow2.weblab.core.model.Resource" fragment="true" partNamespace="resource" />
		</dataFormats>


		<!--======================================================= -->
		<!--========== ROUTE CONTEXT =========== -->
		<!--======================================================= -->
		<route id="warcRoute" streamCache="true">

			<!-- Consuming twitter messages -->
			<from uri="file:src/test/resources/warcs?noop=true" />

			<choice>
				<when>
					<simple>${file:ext} == 'warc.gz' || ${file:ext} == 'warc'</simple>
					<log message="Start consuming ${file:name} using the warc splitter" />

					<split parallelProcessing="false">
						<method bean="warcSplitter" method="splitMessage" />
						<log message="new WARC record... ${headers.ArchiveRecordHttpHeader.protocolStatusCode} - ${headers.ArchiveRecordPayloadHeader['Content-Type']} - ${headers.ArchiveRecordHeader['WARC-Target-URI']}" />

						<choice>
							<when>
								<simple>${headers.ArchiveRecordHttpHeader.protocolStatusCode} range '200..300' and ${headers.ArchiveRecordPayloadHeader['Content-Type']} regex '(.*htm.*)|(.*pdf.*)|(.*msword.*)|(.*rtf.*)|(.*vnd.*)|(.*plain.*)' and ${headers.ArchiveRecordPayloadHeader['Content-Length']} != null</simple>
								<log message="This is valid record (${headers.ArchiveRecordHeader['WARC-Target-URI']}), create a WL resource ..." />

								<!-- A lot of useful metadata is in the Headers of the WARC. A few can be converted to wlp, dc or dct property values -->
								<setHeader headerName="weblab:dc:source">
									<simple>${headers.ArchiveRecordHeader['WARC-Target-URI']}</simple>
								</setHeader>


								<setHeader headerName="weblab:wlp:hasOriginalFileSize">
									<simple>${headers.ArchiveRecordPayloadHeader['Content-Length']}</simple>
								</setHeader>

								<choice>
									<when>
										<simple>${headers.ArchiveRecordPayloadHeader['Date']}</simple>
										<doTry>
											<!-- Try to get the gathering date from the warc. Sometimes a bad date format is added. In that case do not fail but use WARC file date instead. -->
											<setHeader headerName="weblab:wlp:hasGatheringDate">
												<simple resultType="org.ow2.weblab.engine.camel.dataformat.date.RFC2616Date">${headers.ArchiveRecordPayloadHeader['Date']}</simple>
											</setHeader>
											<setHeader headerName="weblab:wlp:hasGatheringDate">
												<simple resultType="java.util.Date">${headers.weblab:wlp:hasGatheringDate.time}</simple>
											</setHeader>
											<doCatch>
												<exception>java.lang.Exception</exception>
												<handled>
													<constant>true</constant>
												</handled>
												<log message="Gathering Date ${headers.ArchiveRecordPayloadHeader['Date']} is not valid for entry ${headers.ArchiveRecordHeader['WARC-Target-URI']}." />
												<setHeader headerName="weblab:wlp:hasGatheringDate">
													<simple resultType="java.util.Date">${header.CamelFileLastModified}</simple>
												</setHeader>
											</doCatch>
										</doTry>
									</when>
									<otherwise>
										<log message="No HTTP date Header for entry ${headers.ArchiveRecordHeader['WARC-Target-URI']}." />
										<setHeader headerName="weblab:wlp:hasGatheringDate">
											<simple resultType="java.util.Date">${header.CamelFileLastModified}</simple>
										</setHeader>
									</otherwise>
								</choice>



								<!-- Last-Modified is optional and might not be present on file. More over it might be a bad date format. In that case, do not fail, just ignore that information. -->
								<filter>
									<simple>${headers.ArchiveRecordPayloadHeader['Last-Modified']} != null</simple>
									<doTry>
										<setHeader headerName="weblab:dct:modified">
											<simple resultType="org.ow2.weblab.engine.camel.dataformat.date.RFC2616Date">${headers.ArchiveRecordPayloadHeader['Last-Modified']}</simple>
										</setHeader>
										<setHeader headerName="weblab:dct:modified">
											<simple resultType="java.util.Date">${headers.weblab:dct:modified.time}</simple>
										</setHeader>
										<doCatch>
											<exception>java.lang.Exception</exception>
											<handled>
												<constant>true</constant>
											</handled>
											<log message="'Modified date ${headers.ArchiveRecordPayloadHeader['Last-Modified']} is not valid for entry ${headers.ArchiveRecordHeader['WARC-Target-URI']}." />
										</doCatch>
									</doTry>
								</filter>


								<!-- Add content type dc:format but everything that might come after ';' (to prevent from text/html;charset=utf8) -->
								<choice>
									<when>
										<simple>${headers.ArchiveRecordPayloadHeader['Content-Type']} contains ';'</simple>
										<!-- Need temporary variable since OGNL does not work on maps... -->
										<setProperty propertyName="TempContentType">
											<simple resultType="java.lang.String">${headers.ArchiveRecordPayloadHeader['Content-Type']}</simple>
										</setProperty>
										<setProperty propertyName="TempIndexOfColumn">
											<simple resultType="java.lang.Integer">${property.TempContentType.indexOf(';')}</simple>
										</setProperty>
										<setHeader headerName="weblab:dc:format">
											<simple>${property.TempContentType.substring(0, property.TempIndexOfColumn)}</simple>
										</setHeader>
									</when>
									<otherwise>
										<setHeader headerName="weblab:dc:format">
											<simple>${headers.ArchiveRecordPayloadHeader['Content-Type']}</simple>
										</setHeader>
									</otherwise>
								</choice>

								<!-- To produce an Text with no content embedded (native content) -->
								<to uri="weblab://create?type=Document&amp;outputMethod=xml" />

								<convertBodyTo type="java.lang.String" />

								<to uri="mock:resultWarcs" />

							</when>
							<otherwise>
								<log message="Ignoring record (${headers.ArchiveRecordHeader['WARC-Target-URI']})" />
							</otherwise>
						</choice>
					</split>
				</when>
				<otherwise>
					<log message="Ignoring file ${file:name} with ext '${file:ext}' It is strange since only 'warc.gz' and 'warc' files are allowed! And nothing more than that is in test resources" />
				</otherwise>
			</choice>
		</route>


	</camelContext>
</spring:beans>