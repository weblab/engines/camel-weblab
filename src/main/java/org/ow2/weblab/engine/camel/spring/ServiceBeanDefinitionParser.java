/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel.spring;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.camel.Endpoint;
import org.apache.camel.component.cxf.CxfComponent;
import org.apache.camel.component.cxf.CxfSpringEndpoint;
import org.springframework.beans.factory.parsing.BeanComponentDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.xml.AbstractSingleBeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.springframework.osgi.service.exporter.support.OsgiServiceFactoryBean;
import org.springframework.util.xml.DomUtils;
import org.w3c.dom.Element;

/**
 * Parse WebLab bean and inject cxf and osgi services with default options.
 * @author asaval
 *
 */
public class ServiceBeanDefinitionParser extends AbstractSingleBeanDefinitionParser {

	private Class<?> weblabServiceInterface;

	public ServiceBeanDefinitionParser(Class<?> weblabServiceInterface){
		this.weblabServiceInterface = weblabServiceInterface;
	}

	@Override
	protected Class<WebLabService> getBeanClass(Element element) {
	      return WebLabService.class;
	   }

	@Override
	protected void doParse(Element element, ParserContext parserContext, BeanDefinitionBuilder bean) {
		// gather info
		String address = element.getAttribute("address");
		String id = element.getAttribute("id");
		Map<String, Object> cxfProperties = toMap(element, "cxfProperties", defaultCxfProperties());
		Map<String, Object> osgiProperties = toMap(element, "osgiProperties", defaultOsgiProperties(id));
		String cxfEndpointBeanName = generateUniqueName(id,"cxf");
	    String osgiServiceBeanName = generateUniqueName(id,"osgi");

		 // complete WebLabService  bean
		 bean.addConstructorArgValue(address);
	     bean.addPropertyValue("id",  id);
	     bean.addPropertyValue("cxfEndpointName", cxfEndpointBeanName);
	     bean.addPropertyValue("osgiServiceName", osgiServiceBeanName);
	     bean.addPropertyValue("cxfProperties", cxfProperties);
	     bean.addPropertyValue("osgiProperties", osgiProperties);

	     // add cxf endpoint
	     BeanDefinitionBuilder bdb = BeanDefinitionBuilder.genericBeanDefinition(CxfSpringEndpoint.class);
	     bdb.addConstructorArgValue(new CxfComponent());
	     bdb.addConstructorArgValue(address);
	     bdb.addPropertyValue("serviceClass", this.weblabServiceInterface.getName());
	     bdb.addPropertyValue("properties", cxfProperties);
	     parserContext.registerBeanComponent(new BeanComponentDefinition(bdb.getBeanDefinition(), cxfEndpointBeanName));

	     //add osgi service
	     bdb = BeanDefinitionBuilder.genericBeanDefinition(OsgiServiceFactoryBean.class);
	     bdb.addPropertyReference("target",cxfEndpointBeanName);
	     bdb.addPropertyValue("interfaces", new Class[]{Endpoint.class});
	     bdb.addPropertyValue("serviceProperties", osgiProperties);
	     parserContext.registerBeanComponent(new BeanComponentDefinition(bdb.getBeanDefinition(), osgiServiceBeanName));

	}

	private static String generateUniqueName(String id, String context) {
		StringBuffer name = new StringBuffer("weblab");
		name.append('_');
		name.append(id);
		name.append('_');
		name.append(context);
		name.append('_');
		name.append(UUID.randomUUID().toString());
		return name.toString();
	}

	protected static Map<String, Object> toMap(final Element element, String name, final Map<String, Object> defaultMap){
			final Element props = DomUtils.getChildElementByTagName(element, name);
		    if (props != null){
		    	List<Element> entries = DomUtils.getChildElementsByTagName(props, "entry");
		    	if (entries != null){
		    		for(Element entry:entries){
				    	defaultMap.put(entry.getAttribute("key"), entry.getAttribute("value"));
				    }
		    	}
		    }
			return defaultMap;
	}

	@SuppressWarnings("unchecked")
	protected Map<String, Object> defaultCxfProperties(){
		final LinkedHashMap<String, Object> properties = new LinkedHashMap<>();
		properties.put("dataFormat", "PAYLOAD");
		properties.put("allowStreaming", "true");
		return (Map<String, Object>)properties.clone();
	}

	@SuppressWarnings("unchecked")
	protected Map<String, Object> defaultOsgiProperties(String id){
		final LinkedHashMap<String, Object> properties = new LinkedHashMap<>();
		properties.put("name", id);
		properties.put("type", this.weblabServiceInterface.getSimpleName());
		return (Map<String, Object>)properties.clone();
	}
}
