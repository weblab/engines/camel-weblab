/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.apache.camel.Exchange;
import org.apache.camel.Expression;
import org.apache.camel.NoTypeConversionAvailableException;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.apache.camel.util.CamelContextHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.annotation.WebLabProperty;
import org.ow2.weblab.annotation.WebLabResource;
import org.ow2.weblab.annotation.WebLabSegment;
import org.ow2.weblab.core.annotator.AbstractAnnotator.Operator;
import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.util.TextUtil;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Segment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.WModelAnnotator;
import org.ow2.weblab.engine.camel.utils.WebLabUtils;
import org.ow2.weblab.rdf.Value;

/**
 * Parse WebLab Annotations to create :
 * <ul>
 * <li>Beans from WebLab Resources, Segments &amp; annotations.</li>
 * <li>WebLab Resources, Segments &amp; annotations from Beans.</li>
 * </ul>
 *
 * It also implements an Aggregation Strategy in order to enrich WebLab Resources with Bean
 *
 * Remark: evaluate only support Document level annotation.
 * In order to retrieve other information, please use camel-sparql
 *
 * @author asaval
 */
public class WebLabBeanParser implements Expression, AggregationStrategy {


	// header
	public static final String WEBLAB_TRANSFORM_TO = "weblab-transform-to";


	Log logger = LogFactory.getLog(WebLabBeanParser.class);


	private Map<String, String> prefixes = WebLabUtils.defaultNamespace();


	private String destinationClass;


	private String prefixMap;


	public WebLabBeanParser() {
		// Nothing to do
	}


	public WebLabBeanParser(final String prefixMap) {
		this.prefixMap = prefixMap;
	}


	public String getDestinationClass() {

		return this.destinationClass;
	}


	public void setDestinationClass(final String destinationClass) {

		this.destinationClass = destinationClass;
	}


	protected void setPrefixes(final Map<String, String> prefixes) {

		this.prefixes = prefixes;
	}


	/**
	 * Parse a bean with WebLab annotation and return a WebLab Resource from bean content
	 *
	 * @param exchange
	 *            the exchange message to be enriched
	 * @param bean
	 *            {@link WebLabResource} annotated object
	 * @param resource
	 *            The resource to be enriched
	 * @return the WebLab Resource
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	public Resource parse(final Exchange exchange, final Object bean, final Resource resource) throws Exception {

		if (bean == null) {
			return null;
		}

		final WebLabResource wrannot = bean.getClass().getAnnotation(WebLabResource.class);
		final List<Field> propertiesToCreate = new LinkedList<>();
		final List<Field> segmentsToCreate = new LinkedList<>();
		final List<Field> mediaUnitToAnnotate = new LinkedList<>();
		final Map<String, Resource> idmap = new HashMap<>();
		final Map<String, String[]> orders = new HashMap<>();

		// create main resource
		if (!wrannot.value().equals(resource.getClass().getName())) {
			throw new InstantiationException("WebLab defined type" + resource.getClass() + " is not the type defined in the Bean: " + wrannot.value());
		}
		// add top resource
		final String mainid = WebLabBeanParser.randomOnEmpty(wrannot.id());
		idmap.put(mainid, resource);
		if (wrannot.resourceIdList().length > 0) {
			orders.put(mainid, wrannot.resourceIdList());
		}

		// list resources and properties to create
		for (final Field field : bean.getClass().getDeclaredFields()) {
			final WebLabResource wra = field.getAnnotation(WebLabResource.class);
			if (wra != null) {
				// if resource
				String id = wra.id();
				if (id.isEmpty()) {
					id = field.getName();
				}
				// Object resourceContent = field.get(bean);
				final Method getter = new PropertyDescriptor(field.getName(), bean.getClass()).getReadMethod();
				final Object resourceContent = getter.invoke(bean);

				if (resourceContent != null) {
					idmap.put(id, WebLabBeanParser.createSubResource(wra.value(), exchange, resourceContent, wra.writeContent()));
					final String[] subids = wra.resourceIdList();
					if (subids.length > 0) {
						orders.put(id, subids);
					}
					// adding
					this.logger.debug("adding resource " + id + " with sub resources " + Arrays.toString(subids));
				} else {
					this.logger.debug("discarding resource creationf " + id + " : no content defined.");
				}
				final WebLabProperty wrp = field.getAnnotation(WebLabProperty.class);
				if (wrp != null) {
					mediaUnitToAnnotate.add(field);
					this.logger.debug("The media unit has property  to put on it!");
				}
			} else {
				// if property
				final WebLabProperty wrp = field.getAnnotation(WebLabProperty.class);
				final WebLabSegment wls = field.getAnnotation(WebLabSegment.class);
				if (wls != null) {
					segmentsToCreate.add(field);
				} else if (wrp != null) {
					propertiesToCreate.add(field);
				} else {
					// if supported bean type
					final WebLabResource wlr = field.getType().getAnnotation(WebLabResource.class);
					if (wlr != null) {
						String id = wlr.id();
						if (id.isEmpty()) {
							id = field.getName();
						}
						// build empty sub resource
						Resource subresource = WebLabUtils.initResource(wlr.value());
						// parse sub bean

						final Method getter = new PropertyDescriptor(field.getName(), bean.getClass()).getReadMethod();
						final Object sub = getter.invoke(bean);

						subresource = this.parse(exchange, sub, subresource);
						if (subresource != null) {
							// add it to the main resource
							idmap.put(id, subresource);
						}
					}
				}
				// if resource is MediaUnit, then write content
				if (field.getName().equals("content") && (resource instanceof MediaUnit)) {
					final Method getter = new PropertyDescriptor(field.getName(), bean.getClass()).getReadMethod();
					final Object content = getter.invoke(bean);

					WebLabUtils.writeContent(exchange, resource, content, true, wrannot.writeContent());
				}
			}
		}

		// add annotations
		this.addSegments(bean, segmentsToCreate, idmap, mainid);

		// add annotations
		this.addAnnotations(bean, propertiesToCreate, idmap, mainid);

		// add annotations on mediaUnit
		this.addAnnotations(bean, mediaUnitToAnnotate, idmap, mainid, true);

		// build document by order first
		final List<String> addedResources = new LinkedList<>();
		for (final Entry<String, String[]> entry : orders.entrySet()) {
			addedResources.addAll(WebLabBeanParser.addResourcesTo(entry.getKey(), entry.getValue(), idmap));
		}

		// remove main resource from idmap
		idmap.remove(mainid);

		// add the rest to the main resource
		for (final Entry<String, Resource> entry : idmap.entrySet()) {
			final String id = entry.getKey();
			if (!addedResources.contains(id)) {
				// add resource to doc
				final Resource res = entry.getValue();
				WebLabBeanParser.addToResource(resource, res);
			}
		}

		return resource;
	}


	private void addSegments(final Object bean, final List<Field> segmentsToCreate, final Map<String, Resource> idmap, final String mainid) throws Exception {

		for (final Field field : segmentsToCreate) {
			final WebLabProperty wrp = field.getAnnotation(WebLabProperty.class);
			final WebLabSegment wls = field.getAnnotation(WebLabSegment.class);

			String resid = wls.mediaUnitId();

			// create segments on resource
			if (resid.equals("")) {
				resid = mainid;
			}

			final Resource res = idmap.get(resid);
			if (res == null) {
				this.logger.warn("No resource with id " + resid + " in " + bean + ". Discarding Segments");
				continue;
			} else if (!(res instanceof MediaUnit)) {
				this.logger.warn("Resource with id " + resid + " in " + bean + " is not a MediaUnit. Discarding Segments");
				continue;
			}

			try {
				final Method getter = new PropertyDescriptor(field.getName(), bean.getClass()).getReadMethod();
				final Object data = getter.invoke(bean);

				final List<Segment> segments = WebLabUtils.createSegments(wls.value(), data, field.getType());

				final WModelAnnotator wma = new WModelAnnotator(res);
				// annotate segments
				for (final Segment segment : segments) {
					final URI segmentURI = URI.create(segment.getUri());

					// add segment to mediaunit
					((MediaUnit) res).getSegment().add(segment);

					// link to the segement
					wma.writeSegment(segmentURI);

					// add annotation on segment
					if ((res instanceof Text) && (segment instanceof LinearSegment)) {
						final WrappedWebLabProperty<String> wwp = this.wrap(wrp, field, bean, mainid);
						wwp.type = String.class;
						wwp.innerSubject = segmentURI;
						try {
							WebLabBeanParser.writeProperty(res, wwp, TextUtil.getSegmentText((Text) res, (LinearSegment) segment));
						} catch (final WebLabCheckedException e) {
							this.logger.warn("Can not write annotation " + wrp + " on text " + ((Text) res).getContent(), e);
						}
					}
				}

			} catch (IllegalArgumentException | IllegalAccessException iae) {
				this.logger.warn("Failed to access " + field.getName() + " on bean " + bean, iae);
			} catch (final ClassNotFoundException cnfe) {
				this.logger.warn("Provided segement class " + wls.value() + " not supported  on field " + field.getName() + " on bean " + bean, cnfe);
			} catch (final IntrospectionException e1) {
				this.logger.warn("Provided segement class " + wls.value() + " not supported  on field " + field.getName() + " on bean " + bean, e1);
			} catch (final InvocationTargetException e1) {
				this.logger.warn("Provided segement class " + wls.value() + " not supported  on field " + field.getName() + " on bean " + bean, e1);
			}
		}

	}


	private void addAnnotations(final Object bean, final List<Field> propertiesToCreate, final Map<String, Resource> idmap, final String mainid) throws Exception {

		this.addAnnotations(bean, propertiesToCreate, idmap, mainid, false);
	}


	private void addAnnotations(final Object bean, final List<Field> propertiesToCreate, final Map<String, Resource> idmap, final String mainid, final boolean targetMediaUnit) throws Exception {

		for (final Field field : propertiesToCreate) {
			final WebLabProperty wrp = field.getAnnotation(WebLabProperty.class);
			String subject = wrp.subjectId();
			if (subject.isEmpty()) {
				subject = mainid;
			}

			// check if resource exist
			final Resource res = idmap.get(subject);
			if (res != null) {
				// wrapped annotation
				final WrappedWebLabProperty<?> wwp = this.wrap(wrp, field, bean, mainid);

				// write annotation
				if (wwp.value != null) {
					if (targetMediaUnit) {// get the MU ID from the IDmap
						WebLabBeanParser.writeProperty(res, wwp, idmap.get(field.getName()).getUri());
					} else {
						WebLabBeanParser.writeProperty(res, wwp, wwp.value);
					}
				} else {
					this.logger.debug("No value defined for " + wrp + " on field " + field + ".  discarding.");
				}

			} else {
				this.logger.warn("No WebLab Resource found with id \"" + subject + "\" for property " + wrp + " on field " + field);
			}
		}
	}


	/**
	 * Add sub resources to resource (mediaunits/annotations)
	 *
	 * @param containerId
	 * @param ids
	 * @param idmap
	 * @return
	 */
	private static List<String> addResourcesTo(final String containerId, final String[] ids, final Map<String, Resource> idmap) {

		final Resource container = idmap.get(containerId);
		// reverse array order
		final List<String> rids = Arrays.asList(ids);
		// Collections.reverse(rids);
		for (final String id : rids) {
			final Resource res = idmap.get(id);
			WebLabBeanParser.addToResource(container, res);
		}
		return rids;
	}


	private static void addToResource(final Resource container, final Resource... subresources) {

		for (final Resource res : subresources) {
			if ((res instanceof MediaUnit) && (container instanceof Document)) {
				((Document) container).getMediaUnit().add((MediaUnit) res);
			} else if (res instanceof Annotation) {
				container.getAnnotation().add((Annotation) res);
			}
		}
	}


	/**
	 * Write one/several properties on a resource
	 *
	 * @param resource
	 * @param wwp
	 * @param data
	 */
	private static <T> void writeProperty(final Resource resource, final WrappedWebLabProperty<T> wwp, final Object data) throws Exception {

		// TODO : support adding annotation in existing annotation & other subjects
		final BaseAnnotator baba = new BaseAnnotator(resource);

		if (wwp.innerSubject != null) {
			baba.startInnerAnnotatorOn(wwp.innerSubject);
		}

		if (!wwp.lang.isEmpty()) {
			baba.startSpecifyLanguage(wwp.lang);
		}

		// check if type is a list
		if ((wwp.type.isAssignableFrom(List.class) && (data instanceof List)) || wwp.type.isArray()) {
			final List<?> list = WebLabUtils.toList(data, wwp.type);

			for (final Object item : list) {
				// convert data to intended type if needed
				final Object value = WebLabUtils.parse(item, item.getClass());
				for (final Property prop : wwp.properties) {
					baba.applyOperator(Operator.WRITE, prop.ns, prop.property, prop.localname, (Class<T>) item.getClass(), (T) value);
				}
			}
		} else {
			final T value = WebLabUtils.parse(data, wwp.type);
			for (final Property prop : wwp.properties) {
				baba.applyOperator(Operator.WRITE, prop.ns, prop.property, prop.localname, wwp.type, value);
			}
		}
	}


	private static String randomOnEmpty(final String string) {

		String result = string;
		if (string.isEmpty()) {
			result = UUID.randomUUID().toString();
		}
		return result;
	}


	/**
	 * Create a resource and write its content
	 *
	 * @param clazz
	 * @param exchange
	 * @param uri
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws NoTypeConversionAvailableException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	private static Resource createSubResource(final String resource, final Exchange exchange, final Object data, final boolean writeContent)
			throws InstantiationException, IllegalAccessException, NoTypeConversionAvailableException, MalformedURLException, IOException {

		final Resource result = WebLabUtils.initResource(resource);
		if (result instanceof Document) {
			throw new InstantiationException("Can not create a WebLab Document in a WebLab Resource");
		} else if (result instanceof MediaUnit) {
			// write resource content
			Object source = data;
			if (data instanceof URI) {
				source = ((URI) data).toURL();
			}
			WebLabUtils.writeContent(exchange, result, source, true, writeContent);
		}
		return result;
	}


	@Override
	public <T> T evaluate(final Exchange exchange, final Class<T> clazz2) {

		Class<?> clazz = exchange.getIn().getHeader(WebLabBeanParser.WEBLAB_TRANSFORM_TO, Class.class);
		final Resource resource = exchange.getIn().getBody(Resource.class);

		// search for prefixes
		if (this.prefixMap != null) {
			final Map<String, String> map = (Map<String, String>) CamelContextHelper.lookup(exchange.getContext(), this.prefixMap);
			if (map != null) {
				this.prefixes.putAll(map);
			}
		}

		if ((clazz == null) && (this.destinationClass != null) && !this.destinationClass.isEmpty()) {
			try {
				clazz = Class.forName(this.destinationClass);
			} catch (final ClassNotFoundException e) {
				this.logger.warn("Tranform Class not found : " + this.destinationClass, e);
			}
		}
		if (clazz == null) {
			clazz = clazz2;
		}

		if (!clazz.isAnnotationPresent(WebLabResource.class)) {
			this.logger.warn("Can not transform WebLab Resource to  " + clazz + ": no WebLabResource annotation found.");
			return null;
		}
		// TODO: check if destination Class is the same as the class in the annotation

		Object result = null;
		try {
			result = clazz.newInstance();
			// search for annotations
			final BaseAnnotator baba = new BaseAnnotator(resource);
			for (final Field field : clazz.getDeclaredFields()) {
				final WebLabProperty wrp = field.getAnnotation(WebLabProperty.class);
				if (wrp != null) {
					final WrappedWebLabProperty<?> wwp = this.wrap(wrp, field, null, resource.getUri());

					if (!wwp.lang.isEmpty()) {
						baba.startSpecifyLanguage(wwp.lang);
					}

					if (!wwp.properties.isEmpty()) {
						final Property p = wwp.properties.get(0);

						Class<?> clazzType = wwp.type;
						if (clazzType.isAssignableFrom(List.class)) {
							// retrieve generic type
							clazzType = (Class<?>) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
						}
						final Value<?> values = baba.applyOperator(Operator.READ, p.ns, p.property, p.localname, clazzType, null);
						Object resultValue = values.firstTypedValue();
						if (values.hasValue()) {
							if (wwp.type.isAssignableFrom(List.class)) {
								// create result list
								final List<Object> list = new LinkedList<>();
								for (final Object item : values) {
									list.add(item);
								}
								resultValue = list;
							}
							final Method setter = new PropertyDescriptor(field.getName(), clazz).getWriteMethod();
							setter.invoke(result, resultValue);
						}
					}
					baba.endSpecifyLanguage();
				}
			}

		} catch (InstantiationException | IllegalAccessException | IntrospectionException | IllegalArgumentException | InvocationTargetException e) {
			this.logger.error("Can not create bean " + clazz, e);
		}

		return (T) result;
	}


	/**
	 * Wrap a WebLabProperty annotation to a more useful Object
	 *
	 * @param wlp
	 * @param field
	 * @param bean
	 * @param mainid
	 * @return
	 */
	private <T> WrappedWebLabProperty<T> wrap(final WebLabProperty wlp, final Field field, final Object bean, final String mainid) {

		final WrappedWebLabProperty<T> wwp = new WrappedWebLabProperty<>();

		// subject
		wwp.subjectId = wlp.subjectId();
		if (wwp.subjectId.isEmpty()) {
			wwp.subjectId = mainid;
		}

		// namespaces
		final String[] namespaces = wlp.value();
		final String[] properties = wlp.property();
		final String[] localnames = wlp.localname();

		wwp.addProperties(namespaces, properties, localnames, field.getName(), this.prefixes);

		// type
		wwp.type = (Class<T>) field.getType();
		// check if simple type
		if (wwp.type.isPrimitive()) {
			wwp.type = (Class<T>) WebLabUtils.primitiveToObject(wwp.type);
		}

		// value
		if (bean != null) {
			try {
				final Method getter = new PropertyDescriptor(field.getName(), bean.getClass()).getReadMethod();
				wwp.value = getter.invoke(bean);
			} catch (IllegalArgumentException | IllegalAccessException | IntrospectionException | InvocationTargetException e) {
				this.logger.error("Can not get value on field " + field, e);
			}
		}

		// language
		wwp.lang = wlp.lang();

		return wwp;
	}


	private class Property {


		public Property() {
			super();
		}


		String ns;


		URI property;


		String localname;
	}


	/**
	 * Wrap WebLabProperty in order to support multiple property annotation
	 *
	 * @author asaval
	 *
	 * @param <T>
	 */
	private class WrappedWebLabProperty<T> {


		public URI innerSubject;


		String subjectId;


		List<Property> properties = new LinkedList<>();


		Class<T> type;


		Object value;


		String lang;


		public WrappedWebLabProperty() {
			super();
		}


		void addProperties(final String[] namespaces, final String properties_p[], final String[] localnames, final String fieldName, final Map<String, String> prefixes_p) {

			// if properties i.e. full namespaces have been defined
			if ((namespaces.length == properties_p.length) && (properties_p.length == localnames.length)) {
				for (int i = 0; i < namespaces.length; i++) {
					this.addProperty(namespaces[i], properties_p[i], localnames[i], fieldName, prefixes_p);
				}
			}
			// if no properties have been furnished, then we assumed that all the namespace are known namespace.
			else if ((namespaces.length == localnames.length) && (properties_p.length == 0)) {
				for (int i = 0; i < namespaces.length; i++) {
					this.addProperty(namespaces[i], "", localnames[i], fieldName, prefixes_p);
				}
			} else {
				final String nsp = namespaces.length > 0 ? namespaces[0] : "";
				final String prop = properties_p.length > 0 ? properties_p[0] : "";
				final String name = localnames.length > 0 ? localnames[0] : "";
				this.addProperty(nsp, prop, name, fieldName, prefixes_p);
			}
		}


		private void addProperty(final String namespace, final String property, final String localname, final String fieldName, final Map<String, String> prefixes_p) {

			final Property innerProperty = new Property();

			innerProperty.ns = namespace;

			// property
			String prop = property;
			if (prop.isEmpty()) {
				// search in prefixes
				prop = prefixes_p.get(innerProperty.ns);
				if (prop == null) {
					WebLabBeanParser.this.logger.warn("Namespace " + innerProperty.ns + " is not supported. "
							+ "To support it, either define it in the WebLabProperty annotation or into WebLab prefixes map (see http://weblab-project.org/index.php?title=Camel-WebLab).");
					return;
				}
			}
			innerProperty.property = URI.create(prop);

			// localName
			innerProperty.localname = localname;
			if (localname.isEmpty()) {
				innerProperty.localname = fieldName;
			}

			this.properties.add(innerProperty);

		}
	}


	@Override
	public Exchange aggregate(final Exchange oldExchange, final Exchange newExchange) {

		if (oldExchange == null) {
			return newExchange;
		}
		final Object oldBody = oldExchange.getIn().getBody();
		final Object newBody = newExchange.getIn().getBody();

		final WebLabResource wra = newBody.getClass().getAnnotation(WebLabResource.class);

		if ((oldBody instanceof Resource) && (wra != null)) {

			if (this.prefixMap != null) {
				final Map<String, String> map = (Map<String, String>) CamelContextHelper.lookup(oldExchange.getContext(), this.prefixMap);
				if (map != null) {
					this.prefixes.putAll(map);
				}
			}

			try {

				Resource newResource;
				if (wra.value().equals(Document.class.getName())) {
					// use WebLab document
					newResource = (Resource) oldBody;
				} else {
					// init a new Resource
					newResource = WebLabUtils.initResource(wra.value());

					// we add the resource to the main resource
					WebLabBeanParser.addToResource((Resource) oldBody, newResource);
				}

				// we parse the bean
				this.parse(newExchange, newBody, newResource);

			} catch (final Exception e) {
				this.logger.error("Aggregation failed: can not parse Bean " + newBody + " into " + wra.value(), e);
			}

		}

		return oldExchange;
	}
}