/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel;

import java.io.ByteArrayOutputStream;
import java.io.StringWriter;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.apache.camel.CamelException;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultProducer;
import org.apache.camel.util.MessageHelper;
import org.ow2.weblab.annotation.WebLabResource;
import org.ow2.weblab.core.annotator.AbstractAnnotator.Operator;
import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.engine.camel.utils.WebLabUtils;

/**
 * The WebLab producer.
 *
 * To perform actions on WebLab resources :
 * - create a new resource
 *
 * To perform a service call for supported interfaces :
 * - analyser, indexer, container, trainable, cleanable
 *
 * To manage contents :
 * -Create, Read, Update, Delete
 *
 * @author emilienbondu
 *
 */
public class WeblabProducer extends DefaultProducer {


	private final WeblabEndpoint endpoint;


	public static final String WEBLAB_SERVICE = "weblab-service";


	public static final String WEBLAB_SERVICE_OSGI_FILTER = "weblab-service-osgi-filter";


	public static final String WEBLAB_SERVICE_CONTEXT = "weblab-service-registry";


	public static final String WEBLAB_SERVICE_SOAP_OPERATION = "weblab-service-soap-operation";


	public static final String DIRECT_REGISTRY = "direct";


	private static final String WEBLAB_SERVICE_EXCEPTION = "weblab-service-exception";


	private static final String WEBLAB_SERVICE_FAILED = "weblab-service-failed";


	private enum SupportedInterface {
		analyser, indexer, container, trainable, cleanable
	}


	public WeblabProducer(final WeblabEndpoint endpoint) {
		super(endpoint);
		this.endpoint = endpoint;
	}


	@Override
	public void process(final Exchange exchange) throws Exception {

		// copying headers
		MessageHelper.copyHeaders(exchange.getIn(), exchange.getOut(), true);

		// filling resources properties
		final Map<String, Object> resourcesProperties = new HashMap<>();
		for (final Entry<String, Object> header : exchange.getIn().getHeaders().entrySet()) {

			if (header.getKey().indexOf(':') <= 0) {
				continue;
			}

			URI uri = null;
			try {
				uri = URI.create(header.getKey());
			} catch (final IllegalArgumentException e) {
				this.log.debug("Invalid URI in header (while it contains a ':' --> " + header.getKey());
				// this is not a valid URI
				continue;
			}

			if ((WeblabEndpoint.SCHEME.equalsIgnoreCase(uri.getScheme())) && (uri.getSchemeSpecificPart() != null)) {
				// add the weblab property (for instance: weblab:dc:source --> dc:source + value)
				resourcesProperties.put(uri.getSchemeSpecificPart(), header.getValue());
			}
		}

		// checking endpoint for action
		final URI endpointURI = URI.create(this.endpoint.getEndpointUri());
		if (endpointURI.getAuthority() != null) {
			if (endpointURI.getAuthority().equalsIgnoreCase("create")) {
				this.create(exchange, resourcesProperties);
			} else if (endpointURI.getAuthority().startsWith("analyser")) {
				this.service(endpointURI, SupportedInterface.analyser, exchange);
			} else if (endpointURI.getAuthority().startsWith("indexer")) {
				this.service(endpointURI, SupportedInterface.indexer, exchange);
			} else if (endpointURI.getAuthority().startsWith("container")) {
				// for now if no operation is specified, an error is throwed.
				if (this.endpoint.getOperation() == null) {
					throw new CamelException("Waiting for an [operation] uri parameter, but none is provided.");
				}
				this.service(endpointURI, SupportedInterface.container, exchange);
			} else if (endpointURI.getAuthority().startsWith("trainable")) {
				// for now if no operation is specified, an error is throwed.
				if (this.endpoint.getOperation() == null) {
					throw new CamelException("Waiting for an [operation] uri parameter, but none is provided.");
				}
				this.service(endpointURI, SupportedInterface.trainable, exchange);
			} else if (endpointURI.getAuthority().startsWith("cleanable")) {
				this.service(endpointURI, SupportedInterface.cleanable, exchange);
			} else if (endpointURI.getAuthority().startsWith("content")) {
				WebLabUtils.manageContent(endpointURI, this.endpoint, exchange);
			} else {
				this.log.error("WebLab Endpoint does not support this URI: " + endpointURI.toString());
			}
		}
	}


	/**
	 * This function removes wrapper around WebLab Resources and manage processing using Registry
	 *
	 * @param endpointURI
	 *            The URI of the endpoint as used in the camel route (something like: "weblab:analyser:gate-extraction")
	 * @param supportedInterface
	 *            The interface (analyser, indexer...) to be used when calling the service
	 * @param exchange
	 *            The exchange received and to be enriched
	 * @throws Exception
	 */
	private void service(final URI endpointURI, final SupportedInterface supportedInterface, final Exchange exchange) throws Exception {
		final String authority = endpointURI.getAuthority();
		final int index = authority.indexOf(':');
		String name = "";
		if (index != -1) {
			name = authority.substring(index + 1);
		}
		if (exchange.hasOut()) {
			exchange.setOut(null);
		}

		final Object originalBody = exchange.getIn().getBody();

		this.log.debug("Calling service " + name + " through " + supportedInterface + " interface.");
		// set the header
		exchange.getIn().setHeader(WeblabProducer.WEBLAB_SERVICE, name);
		exchange.getIn().setHeader(WeblabProducer.WEBLAB_SERVICE_OSGI_FILTER, this.endpoint.getFilter());

		// set operation in exchange properties in order to avoid consuming stream when defining operation name in the route
		exchange.setProperty(WeblabProducer.WEBLAB_SERVICE_SOAP_OPERATION, this.endpoint.getOperation());

		if (WeblabProducer.DIRECT_REGISTRY.equalsIgnoreCase(this.endpoint.getType())) {
			exchange.getIn().setHeader(WeblabProducer.WEBLAB_SERVICE_CONTEXT, exchange.getContext());
		} else {
			// if type != direct, make sure there is on context defined
			exchange.getIn().removeHeader(WeblabProducer.WEBLAB_SERVICE_CONTEXT);
		}

		try {
			// retrieve wrapped endpoint
			final Endpoint theEndpoint = this.endpoint.getCamelContext().getEndpoint("direct-vm:" + supportedInterface);
			theEndpoint.createProducer().process(exchange);
		} catch (final Exception e) {
			// In fact this almost never occurs; most of the time the exception is already wrapped in the exchange
			if (this.endpoint.isFailsafe()) {
				this.failsafe(exchange, name, originalBody, e);
			} else {
				throw e;
			}
		} finally {
			// clean up headers & property
			exchange.setProperty(WeblabProducer.WEBLAB_SERVICE_SOAP_OPERATION, null);
			exchange.getIn().removeHeader(WeblabProducer.WEBLAB_SERVICE);
			exchange.getIn().removeHeader(WeblabProducer.WEBLAB_SERVICE_OSGI_FILTER);
			exchange.getIn().removeHeader(WeblabProducer.WEBLAB_SERVICE_SOAP_OPERATION);
			exchange.getIn().removeHeader(WeblabProducer.WEBLAB_SERVICE_CONTEXT);
		}

		final Exception exception = exchange.getException();
		if ((exception != null) && this.endpoint.isFailsafe()) {
			this.failsafe(exchange, name, originalBody, exception);
		}

	}


	private void failsafe(final Exchange exchange, final String name, final Object originalBody, final Exception exception) {
		this.log.warn("Caught an exception when calling failsafe service " + name + ". Ignoring it.");
		exchange.setOut(null);
		exchange.setException(null);
		exchange.getIn().setBody(originalBody);
		final Object currentExceptionHeader = exchange.getIn().getHeader(WeblabProducer.WEBLAB_SERVICE_EXCEPTION);
		if (currentExceptionHeader == null) {
			exchange.getIn().setHeader(WeblabProducer.WEBLAB_SERVICE_EXCEPTION, currentExceptionHeader + ", " + exception.getMessage());
		} else {
			exchange.getIn().setHeader(WeblabProducer.WEBLAB_SERVICE_EXCEPTION, exception.getMessage());
		}
		Object currentFailedHeader = exchange.getIn().getHeader(WeblabProducer.WEBLAB_SERVICE_FAILED);
		if (currentFailedHeader == null) {
			exchange.getIn().setHeader(WeblabProducer.WEBLAB_SERVICE_FAILED, name);
		} else {
			exchange.getIn().setHeader(WeblabProducer.WEBLAB_SERVICE_FAILED, currentFailedHeader + ", " + name);
		}
	}


	private void create(final Exchange exchange, final Map<String, Object> resourcesProperties) throws Exception {
		// creating a new resource

		Document doc = null;

		Resource resource = WebLabUtils.initResource(this.endpoint.getType());

		exchange.getOut().setHeader("weblabURI", resource.getUri());

		// check if exchange body is annotated with WebLab property
		final Object body = exchange.getIn().getBody();
		WebLabResource wrannot = null;
		if (body != null) {
			wrannot = body.getClass().getAnnotation(WebLabResource.class);
		}
		if (wrannot != null) {
			final WebLabBeanParser wap = new WebLabBeanParser();
			wap.setPrefixes(this.endpoint.getPrefixMap());
			resource = wap.parse(exchange, body, resource);
		}

		if (this.endpoint.isDocumentWrapped() && (resource instanceof MediaUnit)) {
			// adding the unit in a document
			doc = new Document();
			doc.setUri("weblab://" + UUID.randomUUID().toString());
			doc.getMediaUnit().add((MediaUnit) resource);
		}

		// annotating the resource with properties from header
		final BaseAnnotator annotator;
		if (resource instanceof PieceOfKnowledge) {
			annotator = new BaseAnnotator(URI.create(resource.getUri()), (PieceOfKnowledge) resource);
		} else if (doc == null) {
			annotator = new BaseAnnotator(resource);
		} else {
			annotator = new BaseAnnotator(doc);
		}

		// processing resource properties
		for (final Entry<String, Object> property : resourcesProperties.entrySet()) {

			// parsing property Qname, prefix, lang
			final String qn = property.getKey();

			final String prefix = qn.substring(0, qn.indexOf(":"));
			final String localPart = qn.substring(qn.indexOf(":") + 1, qn.length());
			String localPartNoLang = null;
			String lang = null;

			if (localPart.indexOf("@lang=") > 0) {
				lang = localPart.substring((localPart.indexOf("@lang=") + "@lang=".length()), localPart.length());
				localPartNoLang = localPart.substring(0, localPart.indexOf("@lang="));
			}

			// TODO: add an optional feature to check expected value type by property and automatically convert current value to expected type.
			// Let's say you want to annotate a WebLab document with a dc:date, user writes the date as a string (i.e. 2013-09-28) and it is
			// automatically converted to a java.util.Date object using Joda.
			// We should enable this feature for common datatypes used in WL annotator and also custom ones.

			// mapping prefix from endpoint prefix map
			if (this.endpoint.prefixMap.containsKey(prefix)) {

				// create annotations
				if ((lang != null) && (property.getValue() != null)) {
					annotator.startSpecifyLanguage(lang);
					annotator.applyOperator(Operator.WRITE, prefix, URI.create(this.endpoint.prefixMap.get(prefix)), localPartNoLang, Object.class, property.getValue());
					annotator.endSpecifyLanguage();
				} else if (property.getValue() != null) {
					annotator.applyOperator(Operator.WRITE, prefix, URI.create(this.endpoint.prefixMap.get(prefix)), localPart, Object.class, property.getValue());
				} else if (property.getValue() == null) {
					this.log.debug("No value set for property '" + property.getKey() + "'");
				}
			}
		}
		if (wrannot == null) {
			if ((doc == null) || this.endpoint.isWriteContent()) {
				WebLabUtils.writeContent(exchange, resource, body, this.endpoint.filterNonXMLChars, this.endpoint.isWriteContent());
			} else {
				final URI contentUri = WebLabUtils.writeContent(exchange, doc, body, this.endpoint.filterNonXMLChars, this.endpoint.isWriteContent());
				if (contentUri != null) {
					// Also add content annotation to the resource
					new WProcessingAnnotator(resource).writeNativeContent(contentUri);
				}
			}
		}

		// returning resource or document
		if (doc == null) {
			this.outputResource(exchange, resource);
		} else {
			this.outputResource(exchange, doc);
		}
	}


	private void outputResource(final Exchange exchange, final Resource resource) throws WebLabCheckedException {
		// output format
		if ("XML".equalsIgnoreCase(this.endpoint.getOutputMethod())) {
			// marshaling resource as XML
			if (this.endpoint.isStreaming()) {
				final ByteArrayOutputStream out = new ByteArrayOutputStream();
				this.endpoint.getMarshaller().marshalResource(resource, out);
				exchange.getOut().setBody(out);
			} else {
				final StringWriter writer = new StringWriter();
				this.endpoint.getMarshaller().marshalResource(resource, writer);
				exchange.getOut().setBody(writer.toString());
			}
		} else {
			// return resource as object in out body
			exchange.getOut().setBody(resource);
		}
	}

}