/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel.dataformat.date;

import java.util.Date;

import org.joda.time.chrono.ISOChronology;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;


/**
 * This class is a subclass of Date.
 * It is needed in order to create a dedicated converter to be used in camel routes.
 * It enables to convert String that follows the ISO8601 specific date formats into a Date.
 *
 * @author ymombrun
 * @see ISODateTimeFormat#dateOptionalTimeParser()
 */
public class ISO8601Date {


	private final long time;


	/**
	 * @param iso8601Date
	 *            The date in one of the accepted formats in iso8601Date.
	 * @throws IllegalArgumentException
	 *             If the date is not valid against any of the patterns
	 */
	public ISO8601Date(final String iso8601Date) throws IllegalArgumentException {
		this(ISO8601Date.parseDate(iso8601Date));
	}


	/**
	 * Decorates a date by an ISO8601Date
	 *
	 * @param date
	 *            A date to be decorated
	 */
	public ISO8601Date(final Date date) {
		this.time = date.getTime();
	}


	/**
	 * Uses GMT as Timezone
	 *
	 * @param iso8601Date
	 *            The date to be parsed
	 * @return The date parsed
	 * @throws IllegalArgumentException
	 *             If the date cannot be parsed using the JodaTime ISO date parser
	 */
	public static Date parseDate(final String iso8601Date) throws IllegalArgumentException {
		final DateTimeFormatter dtf = ISODateTimeFormat.dateOptionalTimeParser().withChronology(ISOChronology.getInstanceUTC());
		return dtf.parseDateTime(iso8601Date).toDate();
	}


	public long getTime() {
		return this.time;
	}

}
