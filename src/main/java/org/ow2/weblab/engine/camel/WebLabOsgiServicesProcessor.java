/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.Producer;
import org.apache.camel.support.LifecycleStrategySupport;
import org.apache.camel.util.ObjectHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;

/**
 * Processor using OSGi Registry to send exchanges to WebLab OSGi services.
 *
 * @author asaval
 *
 */
public class WebLabOsgiServicesProcessor extends LifecycleStrategySupport implements Processor {


	private final Log logger = LogFactory.getLog(WebLabOsgiServicesProcessor.class);


	private final BundleContext bundleContext;


	public WebLabOsgiServicesProcessor() {
		this.bundleContext = FrameworkUtil.getBundle(WebLabOsgiServicesProcessor.class).getBundleContext();
	}

	/**
	 * Producers cache to avoid
	 */
	private final Map<String, Producer> producersCache = new HashMap<>();

	@Override
	public void process(final Exchange exchange) throws Exception {
		// let's retrieve the header
		final String name = exchange.getIn().getHeader(WeblabProducer.WEBLAB_SERVICE, String.class);
		final String filter = exchange.getIn().getHeader(WeblabProducer.WEBLAB_SERVICE_OSGI_FILTER, String.class);
		final CamelContext context = exchange.getIn().getHeader(WeblabProducer.WEBLAB_SERVICE_CONTEXT, CamelContext.class);
		this.logger.debug("Service name to look up: " + name + " with filter: " + filter);

		// complete filter with given name
		final StringBuffer filterbuffer = new StringBuffer();
		if ((filter == null) || filter.equals("") || (context != null)) {
			filterbuffer.append("(name=");
			filterbuffer.append(name);
			filterbuffer.append(")");
		} else if ((name != null) && !name.equals("")) {
			filterbuffer.append("(&(name=");
			filterbuffer.append(name);
			filterbuffer.append(")");
			filterbuffer.append(URLDecoder.decode(filter, "UTF-8"));
			filterbuffer.append(")");
		}

		// search service in cache
		Producer producer = this.producersCache.get(filterbuffer.toString());
		if (producer == null) {
			// search service in OSGi
			producer = this.searchServiceInRegistry(filterbuffer.toString(), name, context);
			if (producer == null) {
				final Set<Endpoint> set = this.findAllServices(Endpoint.class, null, false);
				throw new WebLabCheckedException("No service with filter " + filterbuffer.toString() + " found. Endpoints as available services are: " + set);
			}
		}
		try {
			// start to process the exchange
			producer.process(exchange);
		} catch (final Exception exception) {
			if (this.producersCache.remove(name) != null) {
				// on exception, we remove producer from cache
				this.logger.info("Removing service " + name + " from cache");
			}
			throw exception;
		}

	}


	/**
	 * Search for the service reference matching the given filter and return a producer on this endpoint
	 *
	 * @param filter
	 *            osgi service reference name
	 * @param context
	 *            if not null, it will search for the endpoint in this context
	 * @return a producer on the endpoint ou null if the service was not found.
	 * @throws Exception
	 */
	private Producer searchServiceInRegistry(final String filter, final String name, final CamelContext context) throws Exception {
		final Endpoint endpoint;
		if (context == null) {
			// search for endpoint among OSGi services
			endpoint = this.searchEndpoint(filter);
		} else {
			// search endpoint in given context
			endpoint = context.getEndpoint(name);
		}

		Producer producer = null;

		if (endpoint != null) {
			this.logger.debug("Endpoint found matching filter " + filter + " " + endpoint);
			// start the endpoint
			endpoint.start();
			// create a producer
			producer = endpoint.createProducer();
			// starting it
			producer.start();
			// add producer in cache
			this.producersCache.put(filter, producer);
		}

		return producer;
	}


	/**
	 * Return a Endpoint whose service matches the given filter
	 *
	 * @param filter
	 *            a filter that services referencing Endpoint must match
	 * @return a Endpoint whose service matches the given filter
	 */
	private Endpoint searchEndpoint(final String filter) {
		final Set<Endpoint> endpoints = this.findAllServices(Endpoint.class, filter, true);
		Endpoint endpoint = null;
		if (!endpoints.isEmpty()) {
			endpoint = endpoints.iterator().next();
		}
		return endpoint;
	}


	/**
	 * Find one or several services of a given Class matching the filter.
	 *
	 * @param clazz
	 *            services must be declared with this class
	 * @param filter
	 *            services must match this osgi service filter
	 * @param first
	 *            if true, it will return only the first service found
	 * @return one or several services of a given Class matching the filter.
	 */
	private <T> Set<T> findAllServices(final Class<T> clazz, final String filter, final boolean first) {
		final Set<T> endpoints = new HashSet<>(1);
		try {
			this.logger.info("Osgi Registry lookup on " + filter);
			final ServiceReference<?>[] refs = this.bundleContext.getServiceReferences(clazz.getName(), filter);
			if ((refs != null) && (refs.length > 0)) {
				if (first) {
					// just return the first one
					final ServiceReference<?> sr = refs[0];
					final Object service = this.bundleContext.getService(sr);
					endpoints.add(clazz.cast(service));
				} else {
					for (final ServiceReference<?> sr : refs) {
						final Object service = this.bundleContext.getService(sr);
						endpoints.add(clazz.cast(service));
					}
				}
			}
		} catch (final Exception ex) {
			throw ObjectHelper.wrapRuntimeCamelException(ex);
		}
		return endpoints;
	}


	@Override
	public void onContextStop(final CamelContext context) {
		// Clean up Producer cache
		this.producersCache.clear();
	}

}