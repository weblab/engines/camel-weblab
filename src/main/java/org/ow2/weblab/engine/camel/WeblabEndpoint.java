/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Consumer;
import org.apache.camel.Processor;
import org.apache.camel.Producer;
import org.apache.camel.impl.DefaultEndpoint;
import org.apache.camel.spi.UriParam;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.engine.camel.utils.WebLabUtils;

/**
 * Represents a Weblab endpoint.
 *
 * @author emilienbondu
 *
 */
public class WeblabEndpoint extends DefaultEndpoint {


	@UriParam
	private String action;


	@UriParam
	private String outputMethod;


	@UriParam
	private boolean streaming;


	@UriParam
	private String type;


	@UriParam
	private boolean writeContent;


	@UriParam
	boolean filterNonXMLChars = true;


	@UriParam
	private String filter = "";


	@UriParam
	private boolean documentWrapped = false;


	@UriParam
	private String operation = null;


	@UriParam
	private boolean failsafe = false;


	public Map<String, String> prefixMap;


	public static final String SCHEME = "weblab";


	private WebLabMarshaller marshaller = new WebLabMarshaller();


	public WeblabEndpoint() {
		if (this.prefixMap == null) {
			this.prefixMap = new HashMap<>();
		}
		this.prefixMap.putAll(WebLabUtils.defaultNamespace());
	}


	public WeblabEndpoint(final String uri, final WeblabComponent component) {
		super(uri, component);

		if (this.prefixMap == null) {
			this.prefixMap = new HashMap<>();
		}
		this.prefixMap.putAll(WebLabUtils.defaultNamespace());
	}


	@Deprecated
	public WeblabEndpoint(final String endpointUri) {
		super(endpointUri);
	}


	@Override
	public Producer createProducer() throws Exception {
		return new WeblabProducer(this);
	}


	@Override
	public Consumer createConsumer(final Processor processor) throws Exception {
		return null;
	}


	@Override
	public boolean isSingleton() {
		return true;
	}


	public String getAction() {
		return this.action;
	}


	public void setAction(final String action) {
		this.action = action;
	}


	public String getOutputMethod() {
		return this.outputMethod;
	}


	public void setOutputMethod(final String outputMethod) {
		this.outputMethod = outputMethod;
	}


	public String getFilter() {
		return this.filter;
	}


	public void setFilter(final String filter) {
		this.filter = filter;
	}


	public boolean isStreaming() {
		return this.streaming;
	}


	public void setStreaming(final boolean streaming) {
		this.streaming = streaming;
	}


	public Map<String, String> getPrefixMap() {
		return this.prefixMap;
	}


	public void setPrefixMap(final Map<String, String> prefixMap) {
		if (prefixMap != null) {
			this.prefixMap.putAll(prefixMap);
		}
	}


	public String getType() {
		return this.type;
	}


	public void setType(final String type) {
		this.type = type;
	}


	public boolean isWriteContent() {
		return this.writeContent;
	}


	public void setWriteContent(final boolean writeContent) {
		this.writeContent = writeContent;
	}


	public boolean isDocumentWrapped() {
		return this.documentWrapped;
	}


	public void setDocumentWrapped(final boolean documentWrapped) {
		this.documentWrapped = documentWrapped;
	}


	public String getOperation() {
		return this.operation;
	}


	public void setOperation(final String operation) {
		this.operation = operation;
	}


	public WebLabMarshaller getMarshaller() {
		return this.marshaller;
	}


	public void setMarshaller(final WebLabMarshaller marshaller) {
		this.marshaller = marshaller;
	}


	/**
	 * @return the failsafe
	 */
	public boolean isFailsafe() {
		return this.failsafe;
	}


	/**
	 * @param failsafe
	 *            the failsafe to set
	 */
	public void setFailsafe(final boolean failsafe) {
		this.failsafe = failsafe;
	}

}
