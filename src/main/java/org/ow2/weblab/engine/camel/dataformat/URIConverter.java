/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel.dataformat;

import java.net.URI;
import java.net.URISyntaxException;

import javax.xml.transform.TransformerException;

import org.apache.camel.Converter;
import org.apache.camel.converter.jaxp.XmlConverter;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * A Camel URI converter based on http://www.ietf.org/rfc/rfc2396.txt with the Java deviations on URi(String str) constructor.
 *
 * @see "http://docs.oracle.com/javase/7/docs/api/java/net/URI.html#URI%28java.lang.String%29"
 */
@Converter
public class URIConverter {


	@Converter
	public static URI convertTo(final String value) throws TransformerException {
		if (value == null) {
			throw new TransformerException("URIConverter can not convert null String to URI.");
		}
		try {
			return new URI(value);
		} catch (final URISyntaxException use) {
			throw new TransformerException("URIConvert failed to create URI from: " + value, use);
		}
	}



	/**
	 * Convert a XML Node to an URI.
	 *
	 * Here is an implementation choice
	 * The XmlConverter return the empty String when the node is empty
	 * Due to java deviation from the RCF on URI(String str) constructor, it allows to create an empty URI.
	 * When it makes sense when converting an empty XML node to an empty string,
	 * There is no such thing as an empty URI outside Java deviations.
	 * We choose to send a TransformerException when the given node is empty,
	 *
	 * Watch out: The method URI convertTo(String value) allows to create an "empty URI", since the String parameter is a direct request to convert an empty String to an URI.
	 *
	 * @see "http://docs.oracle.com/javase/7/docs/api/java/net/URI.html#URI%28java.lang.String%29"
	 * @param node
	 *            the {@link Node} that should contain an URI
	 * @return The URI
	 * @throws TransformerException
	 *             if the XML is an empty node or the text from the node is a not an URI.
	 */
	@Converter
	public static URI convertTo(final Node node) throws TransformerException {
		if (node == null) {
			throw new TransformerException("URIConverter can not convert null org.w3c.dom.Node to URI.");
		}
		final XmlConverter converter = new XmlConverter();
		final String data = converter.toString(node, null);

		// the implementation choice is here !
		if (data != null && "".equals(data)) {
			throw new TransformerException("URIConverter does not support converting an empty XML node to an empty URI."
					+ "You may convert the XML node into String if you want to create an empty URI from an empty String.");
		}
		return URIConverter.convertTo(data);
	}


	@Converter
	public static URI convertTo(final NodeList nodelist) throws TransformerException {
		if (nodelist == null) {
			throw new TransformerException("URIConverter can not convert null org.w3c.dom.NodeList to URI.");
		}
		final XmlConverter converter = new XmlConverter();
		return URIConverter.convertTo(converter.toDOMNodeFromSingleNodeList(nodelist));
	}

}