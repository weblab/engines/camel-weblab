/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.camel.Body;
import org.apache.camel.Headers;
import org.apache.camel.Message;
import org.apache.camel.impl.DefaultMessage;
import org.apache.commons.io.IOUtils;
import org.jwat.common.HeaderLine;
import org.jwat.warc.WarcConstants;
import org.jwat.warc.WarcReader;
import org.jwat.warc.WarcReaderFactory;
import org.jwat.warc.WarcRecord;

/**
 * To split a WARC file from a Camel body message to ArchiveRecords.
 * Each WARC archive content is added to the response body. Each WARC record headers are added to the response header '{@value #ARCHIVE_RECORD_HEADER}'.
 * When available, the http header of the record is added to the response header ' {@value #ARCHIVE_HTTP_HEADER}'.
 * When available, the metadata values associated to a target URI of the record is added to the response header ' {@value #ARCHIVE_METADATA_PAYLOAD_HEADER}'.
 * All available record payload headers are added to the response header '{@value #ARCHIVE_PAYLOAD_HEADER}'.
 *
 * @author emilienbondu, ymombrun
 */
public class WarcSplitter {


	/**
	 * The Key of the Camel Header that contains the headers Map of the record contained in the message.
	 */
	public static final String ARCHIVE_RECORD_HEADER = "ArchiveRecordHeader";


	/**
	 * The Key of the Camel Header that contains the HTTP headers Map of the record contained in the message.
	 */
	public static final String ARCHIVE_HTTP_HEADER = "ArchiveRecordHttpHeader";


	/**
	 * The Key of the Camel Header that contains the map of values described in the payload of a metadata record associated with the message.
	 */
	public static final String ARCHIVE_METADATA_PAYLOAD_HEADER = "ArchiveMetadataPayloadHeader";


	/**
	 * The Key of the Camel Header that contains the map of values described in the payload of the message (In case of payload header wrapped).
	 */
	public static final String ARCHIVE_PAYLOAD_HEADER = "ArchiveRecordPayloadHeader";


	/**
	 * The Key of the Camel Header that contains the map of values described in the payload of the warcinfo record of the WARC file.
	 */
	public static final String ARCHIVE_INFO_PAYLOAD_HEADER = "ArchiveInfoPayloadHeader";


	/**
	 * Whether or not to transfer as Camel Header the properties described in the payload of the warcinfo record at the begining of the WARC file.
	 */
	private boolean copyWarcInfoHeaders = false;


	/**
	 * Whether or not to transfer as Camel Header the properties described in the payload of the (optional) metadata record associated with the record of the message.
	 */
	private boolean copyMetadataHeaders = false;


	/**
	 * Can be used to restrict the records to be output to a given WARC-Type. Default value is to let them all pass (when set is empty).
	 */
	private Set<String> forwardedRecords = new HashSet<>();


	/**
	 * To split the WARC file in WARC records.
	 *
	 * @param warcFile
	 *            The warc file to be splited into messages
	 * @param headers
	 *            The header describing the file as provided by the camel route
	 * @return the list of messages corresponding to archive records
	 * @throws IOException
	 *             if not a valid WARC file
	 */
	public List<Message> splitMessage(@Body final File warcFile, @Headers final Map<String, Object> headers) throws IOException {
		// the list to return
		final List<Message> answer = new ArrayList<>();

		// warcinfo headers map
		final Map<String, Object> warcinfoHeaders = new HashMap<>();

		// map of metadata map per WARC-Record-ID (linked using either concurrent or refers to fields)
		final Map<String, Map<String, Object>> metadataHeaders = new HashMap<>();

		// If one of additional info is activated, read the warc once to extract metadata before really splitting the records
		if (this.copyMetadataHeaders || this.copyWarcInfoHeaders) {
			try (final FileInputStream fis = new FileInputStream(warcFile)) {
				final Iterator<WarcRecord> records = WarcReaderFactory.getReader(fis).iterator();

				while (records.hasNext()) {
					final WarcRecord record = records.next();

					// Only open not empty payloads of type warc-fields (key: value)
					if (WarcSplitter.hasNotEmptyPayload(record) && WarcSplitter.safeHeaderEquals(record, WarcConstants.FN_CONTENT_TYPE, WarcConstants.CT_APP_WARC_FIELDS)) {

						if (this.copyWarcInfoHeaders && WarcSplitter.safeHeaderEquals(record, WarcConstants.FN_WARC_TYPE, WarcConstants.RT_WARCINFO)) {
							WarcSplitter.fillHeaderMap(warcinfoHeaders, record);

							/*
							 * There is only one warcinfo per warc.
							 * Saving time by breaking the loop if useful (no need to extract metadata for nothing).
							 */
							if (!this.copyMetadataHeaders) {
								break;
							}
						} else if (this.copyMetadataHeaders && WarcSplitter.safeHeaderEquals(record, WarcConstants.FN_WARC_TYPE, WarcConstants.RT_METADATA)) {
							// According to the specifications, two fields can be used for the same thing: concurrent to and refers to. Check for both.
							final Map<String, Object> metadataRecordHeaders = new HashMap<>();
							WarcSplitter.fillHeaderMap(metadataRecordHeaders, record);
							if (record.getHeader(WarcConstants.FN_WARC_CONCURRENT_TO) != null) {
								metadataHeaders.put(record.getHeader(WarcConstants.FN_WARC_CONCURRENT_TO).value, metadataRecordHeaders);
							}
							if (record.getHeader(WarcConstants.FN_WARC_REFERS_TO) != null) {
								metadataHeaders.put(record.getHeader(WarcConstants.FN_WARC_REFERS_TO).value, metadataRecordHeaders);
							}
						}

					} // else empty record, ignoring it.
				} // end of while
			} // end of try
		}


		try (FileInputStream fis = new FileInputStream(warcFile)) {
			final WarcReader reader = WarcReaderFactory.getReader(fis);
			final Iterator<WarcRecord> records = reader.iterator();

			while (records.hasNext()) {
				final WarcRecord record = records.next();

				if (WarcSplitter.hasNotEmptyPayload(record) && (this.forwardedRecords.isEmpty() || this.forwardedRecords.contains(record.getHeader(WarcConstants.FN_WARC_TYPE).value))) {

					// creating a new message and adding archive headers
					final Message message = new DefaultMessage();

					// adding archive record header map as header
					final Map<String, String> archiveRecordHeader = new HashMap<>();
					for (final HeaderLine hl : record.getHeaderList()) {
						archiveRecordHeader.put(hl.name, hl.value);
					}
					message.setHeader(WarcSplitter.ARCHIVE_RECORD_HEADER, archiveRecordHeader);

					// adding archive record http header as header
					if (record.getHttpHeader() != null) {
						message.setHeader(WarcSplitter.ARCHIVE_HTTP_HEADER, record.getHttpHeader());
					}

					// adding archive record payload header map as header
					final Map<String, String> payloadHeaders = new HashMap<>();
					if (record.getPayload().getPayloadHeaderWrapped() != null) {
						for (final HeaderLine l : record.getPayload().getPayloadHeaderWrapped().getHeaderList()) {
							payloadHeaders.put(l.name, l.value);
						}
					}
					message.setHeader(WarcSplitter.ARCHIVE_PAYLOAD_HEADER, payloadHeaders);

					// add previous headers don't use the setHeaders method
					for (final Entry<String, Object> entry : headers.entrySet()) {
						message.setHeader(entry.getKey(), entry.getValue());
					}


					// add warc info headers on each message
					if (this.copyWarcInfoHeaders) {
						message.setHeader(WarcSplitter.ARCHIVE_INFO_PAYLOAD_HEADER, warcinfoHeaders);
					}

					// add metadata headers on each message
					if (this.copyMetadataHeaders) {
						final Map<String, Object> metadataRecordHeaders = metadataHeaders.get(record.getHeader(WarcConstants.FN_WARC_RECORD_ID).value);
						if (metadataRecordHeaders != null) {
							message.setHeader(WarcSplitter.ARCHIVE_METADATA_PAYLOAD_HEADER, metadataRecordHeaders);
						}
					}

					// copying archive content to the response body
					final ByteArrayOutputStream bos = new ByteArrayOutputStream();
					IOUtils.copyLarge(record.getPayloadContent(), bos);
					message.setBody(bos);

					// add message to list
					answer.add(message);
				} // End of not handled record type
			} // End of while

		} // End of try

		return answer;
	}


	/**
	 * @param record
	 *            The record to check if it has a non empty payload
	 * @return <code>true</code> if <code>record</code> has a non empty payload
	 */
	private static boolean hasNotEmptyPayload(final WarcRecord record) {
		return record.hasPayload() && (record.getPayload().getTotalLength() > 0);
	}


	/**
	 * @param record
	 *            The record from which to read header
	 * @param headerKey
	 *            The key of the reader to read
	 * @param headerExpectedValue
	 *            The value against which to test
	 * @return
	 */
	private static boolean safeHeaderEquals(final WarcRecord record, final String headerKey, final String headerExpectedValue) {
		final HeaderLine header = record.getHeader(headerKey);
		return (header != null) && headerExpectedValue.equalsIgnoreCase(header.value);
	}


	/**
	 * @param headerMap
	 *            The map to be enriched with key/value extracted from the payload of the warc record
	 * @param warcFieldsRecord
	 *            The warc record to be read
	 * @throws IOException
	 */
	private static void fillHeaderMap(final Map<String, Object> headerMap, final WarcRecord warcFieldsRecord) throws IOException {
		final List<String> payloadLines = IOUtils.readLines(warcFieldsRecord.getPayloadContent(), StandardCharsets.UTF_8);

		// trying to parse each payload line as a header name/value pair
		for (final String line : payloadLines) {
			final int first = line.indexOf(":");
			if (first > 0) {
				final String key = line.substring(0, first).trim();

				// Fix for WEBLAB-1292. Key are not unique (especially outlinks)
				if (headerMap.containsKey(key)) {
					if (headerMap.get(key) instanceof String) {
						final LinkedList<String> lst = new LinkedList<>();
						lst.add((String) headerMap.get(key));
						headerMap.put(key, lst);
					}
					final Object currentValue = headerMap.get(key);
					if (currentValue instanceof List) {
						((List<String>) currentValue).add(line.substring(first + 1).trim());
					}
				} else {
					headerMap.put(key, line.substring(first + 1).trim());
				}
			}
		}
	}


	/**
	 * To get the copyWarcInfoHeaders flag. If true, warcinfo headers are included in each split message. Warcinfo headers are copied in the header "{@value #ARCHIVE_INFO_PAYLOAD_HEADER}"
	 *
	 * @return The copyWarcInfoHeaders value
	 */
	public boolean isCopyWarcInfoHeaders() {
		return this.copyWarcInfoHeaders;
	}


	/**
	 * To set the copyWarcInfoHeaders flag. If true, warcinfo headers are included in each split message. Warcinfo headers are copied in the header "{@value #ARCHIVE_INFO_PAYLOAD_HEADER}"
	 *
	 * @param copyWarcInfoHeaders
	 *            The copyWarcInfoHeaders to set
	 */
	public void setCopyWarcInfoHeaders(final boolean copyWarcInfoHeaders) {
		this.copyWarcInfoHeaders = copyWarcInfoHeaders;
	}


	/**
	 * To get the copyMetadataHeaders flag. If true, metadata payload headers are included in each split message. Metadata headers are copied in the header "{@value #ARCHIVE_METADATA_PAYLOAD_HEADER}"
	 *
	 * @return the copyMetadataHeaders value
	 */
	public boolean isCopyMetadataHeaders() {
		return this.copyMetadataHeaders;
	}


	/**
	 * To set the copyMetadataHeaders flag. If true, metadata payload headers are included in each split message. Metadata headers are copied in the header "{@value #ARCHIVE_METADATA_PAYLOAD_HEADER}"
	 *
	 * @param copyMetadataHeaders
	 *            the copyMetadataHeaders to set
	 */
	public void setCopyMetadataHeaders(final boolean copyMetadataHeaders) {
		this.copyMetadataHeaders = copyMetadataHeaders;
	}


	/**
	 * To get the forwardedRecords set. If empty, all type of records are converted to messages. Otherwise, only create message entry for the selected types.
	 *
	 * @return the forwardedRecords
	 */
	public Set<String> getForwardedRecords() {
		return this.forwardedRecords;
	}


	/**
	 * To set the forwardedRecords set. If empty, all type of records are converted to messages. Otherwise, only create message entry for the selected types.
	 * Default value is an empty set.
	 *
	 * @param forwardedRecords
	 *            the forwardedRecords to set
	 */
	public void setForwardedRecords(final Set<String> forwardedRecords) {
		this.forwardedRecords = forwardedRecords;
	}

}
