/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel.dataformat;

import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

import javax.xml.transform.TransformerException;

import org.apache.camel.Converter;
import org.apache.commons.io.IOUtils;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Resource;

@Converter
public class ResourceConverter {


	@Converter
	public static InputStream convertTo(final Resource resource) throws TransformerException {
		if (resource == null) {
			throw new TransformerException("ResourceConverter can not convert null to a WebLab Resource.");
		}

		try {
			final StringWriter writer = new StringWriter();
			new WebLabMarshaller().marshalResource(resource, writer);
			return IOUtils.toInputStream(writer.toString(), StandardCharsets.UTF_8);
		} catch (final WebLabCheckedException exception) {
			throw new TransformerException("Failed to convert stream to WebLab Resource due to:" + exception.getMessage(), exception);
		}
	}


	@Converter
	public static Resource convertTo(final InputStream stream) throws TransformerException {
		if (stream == null) {
			throw new TransformerException("ResourceConverter can not convert null to a WebLab Resource.");
		}

		try {
			return new WebLabMarshaller().unmarshal(stream, Resource.class);
		} catch (final WebLabCheckedException exception) {
			throw new TransformerException("Failed to convert stream to WebLab Resource due to:" + exception.getMessage(), exception);
		}
	}


	@Converter
	public static Resource convertTo(final StringWriter writer) throws TransformerException {
		if (writer == null) {
			throw new TransformerException("ResourceConverter can not convert null to a WebLab Resource.");
		}

		try {
			return new WebLabMarshaller().unmarshal(new StringReader(writer.toString()), Resource.class);
		} catch (final WebLabCheckedException exception) {
			throw new TransformerException("Failed to convert stream to WebLab Resource due to:" + exception.getMessage(), exception);
		}
	}

}
