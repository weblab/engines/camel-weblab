/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel.spring;

import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.Cleanable;
import org.ow2.weblab.core.services.Indexer;
import org.ow2.weblab.core.services.ResourceContainer;
import org.ow2.weblab.core.services.Trainable;
import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

/**
 * WebLab namespace handle, it is enabling elements defined in weblab-spring.xsd.
 *
 * @author asaval
 *
 */
public class WebLabNamespaceHandler extends NamespaceHandlerSupport {

	@Override
	public void init() {
		// init supported WebLab service interfaces bean parser
		registerBeanDefinitionParser("analyser", new ServiceBeanDefinitionParser(Analyser.class));
		registerBeanDefinitionParser("indexer", new ServiceBeanDefinitionParser(Indexer.class));
		registerBeanDefinitionParser("container", new ServiceBeanDefinitionParser(ResourceContainer.class));
		registerBeanDefinitionParser("trainable", new ServiceBeanDefinitionParser(Trainable.class));
		registerBeanDefinitionParser("cleanable", new ServiceBeanDefinitionParser(Cleanable.class));
	}
}