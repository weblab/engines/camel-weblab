/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel.dataformat;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.apache.camel.Converter;
import org.ow2.weblab.core.services.cleanable.CleanArgs;

/**
 * To convert body to CleanArgs (as an object or an XML stream).
 *
 * @author ebondu
 *
 */
@Converter
public class CleanArgsConverter {


	/**
	 * To convert a CleanArg to a XML stream
	 *
	 * @param ca
	 *            cleanArg
	 * @return a InputStream on the XML
	 * @throws JAXBException if an error occurs with the marshalling of the {@link CleanArgs} into an {@link InputStream}
	 */
	@Converter
	public InputStream convertTo(final CleanArgs ca) throws JAXBException {
		final StringWriter writer = new StringWriter();

		final JAXBContext jaxbContext = JAXBContext.newInstance(CleanArgs.class);
		final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// format the XML output
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

		final QName qName = new QName("http://weblab.ow2.org/core/1.2/services/cleanable", "cleanArgs");
		final JAXBElement<CleanArgs> root = new JAXBElement<>(qName, CleanArgs.class, ca);

		final Marshaller m = jaxbContext.createMarshaller();
		m.marshal(root, writer);

		return new ByteArrayInputStream(writer.toString().getBytes());
	}


	/**
	 * To convert a Collection of objects to a CleanArg.
	 * The toString() method of the object is used to get the resourceId.
	 *
	 * @param resourceIds The list of resource id to be cleaned (removed)
	 * @return a cleanArg
	 */
	@Converter
	public CleanArgs convertTo(final Collection<?> resourceIds) {

		final CleanArgs ca = new CleanArgs();
		final List<String> uris = new ArrayList<>();
		for (final Object uri : resourceIds) {
			uris.add(uri.toString());
		}

		ca.getTrash().addAll(uris);

		return ca;
	}


	/**
	 * To convert an object to a CleanArg.
	 * The toString() method of the object is used to get the resourceId.
	 *
	 * @param resourceId
	 *            The id of the resource to be added
	 * @return a cleanArg
	 */
	@Converter
	public CleanArgs convertTo(final Object resourceId) {
		final CleanArgs ca = new CleanArgs();
		ca.getTrash().add(resourceId.toString());

		return ca;
	}
}
