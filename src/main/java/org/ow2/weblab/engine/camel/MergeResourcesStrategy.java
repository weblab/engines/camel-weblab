/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel;

import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;

import org.apache.camel.Exchange;
import org.apache.camel.RuntimeCamelException;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.components.ResourcesMerger;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Resource;

/**
 * The strategy allows to merge WebLab Resources.
 *
 * @author asaval
 *
 */
public class MergeResourcesStrategy implements AggregationStrategy {


	private final Log logger = LogFactory.getLog(MergeResourcesStrategy.class);


	private final ResourcesMerger resourceMerger = ResourcesMerger.getInstance();


	@Override
	public Exchange aggregate(final Exchange oldExchange, final Exchange newExchange) {

		if (oldExchange == null) {
			return newExchange;
		}

		final Resource resource1 = this.extractBody(oldExchange);
		final Resource resource2 = this.extractBody(newExchange);

		this.logger.info("Merging resources: " + resource1.getUri() + " " + resource2.getUri());
		oldExchange.getIn().setBody(this.resourceMerger.merge(resource1, resource1, resource2));
		return oldExchange;
	}


	/**
	 * Extracts the body of an {@link Exchange} and convert it into a {@link Resource}.
	 *
	 * @param exchange
	 *            The Camel {@link Exchange} that contain a streamed WebLab {@link Resource}
	 *
	 * @return The result of the unmarshalling of the {@link Resource} embeded in the {@code exchange}.
	 */
	private Resource extractBody(final Exchange exchange) {

		final Object body = exchange.getIn().getBody();
		final Resource resource;
		try {
			if (body == null) {
				final String message = "Body was null. Unable to extract a resource to merge.";
				this.logger.error(message);
				throw new RuntimeCamelException(message);
			} else if (body instanceof Resource) {
				resource = (Resource) body;
			} else if (body instanceof String) {
				resource = new WebLabMarshaller().unmarshal(new StringReader((String) body), Resource.class);
			} else if (body instanceof Reader) {
				resource = new WebLabMarshaller().unmarshal((Reader) body, Resource.class);
			} else if (body instanceof InputStream) {
				resource = new WebLabMarshaller().unmarshal((InputStream) body, Resource.class);
			} else {
				this.logger.debug("Body is of type " + body.getClass() + " try to convert it to Strean and extract a resource from that.");
				resource = new WebLabMarshaller().unmarshal(exchange.getIn().getBody(InputStream.class), Resource.class);
			}
		} catch (final WebLabCheckedException wlce) {
			throw new RuntimeCamelException("Failed to convert into a WebLab resource the body of the exchange : " + exchange.toString(), wlce);
		}

		if (resource == null) {
			throw new RuntimeCamelException("Failed to convert into a WebLab resource the body of the exchange : " + exchange.toString());
		}
		return resource;
	}

}
