/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel.dataformat;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;

import javax.xml.transform.TransformerException;

import org.apache.camel.Converter;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.ow2.weblab.engine.camel.dataformat.date.ISO8601Date;
import org.ow2.weblab.engine.camel.dataformat.date.RFC2616Date;

/**
 * A Camel date converter based on JodaTime lib to convert any instant thing into Date with the following deviation:
 * <ul>
 * <li>If the value is null, then a TransformerException is thrown instead of return the current date</li>
 * </ul>
 *
 * @author emilienbondu, asaval, ymombrun
 */
@Converter
public class DateConverter {

	/**
	 * @param value
	 *            Any object that could be converted to a Date using Joda DateTime default constructor (Long, Date, Calendar, String in ISO format)
	 * @return The Date represented by the value
	 * @throws TransformerException
	 *             If a converter of the given type cannot be found, if the String does not follow ISO date format or if value is null
	 */
	@Converter
	public static Date objectToDate(final Object value) throws TransformerException {
		if (value == null) {
			throw new TransformerException("DateConverter does not support null value.");
		}
		try {
			return new DateTime(value, DateTimeZone.UTC).toDate();
		} catch (final Exception exception) {
			throw new TransformerException(exception);
		}
	}

	@Converter
	public static Timestamp dateToTimestamp(final Date date) throws TransformerException {
		if (date == null) {
			throw new TransformerException("Can not tranform a null Date into a Timestamp.");
		}
		return new Timestamp(date.getTime());
	}

	@Converter
	public static Timestamp stringToTimestamp(final String date) throws TransformerException {
		return dateToTimestamp(objectToDate(date));
	}

	@Converter
	public static RFC2616Date dateToRFC2616Date(final Date date) throws TransformerException {
		if (date == null) {
			throw new TransformerException("Can not tranform a null Date into an RFC2616Date.");
		}
		return new RFC2616Date(date);
	}

	@Converter
	public static RFC2616Date stringToRFC2616Date(final String dateUsingRFC2616format) throws TransformerException {
		if (dateUsingRFC2616format == null) {
			throw new TransformerException("Can not tranform a null String into an RFC2616Date.");
		}
		try {
			return new RFC2616Date(dateUsingRFC2616format);
		} catch (final ParseException pe) {
			throw new TransformerException(pe);
		}
	}

	@Converter
	public static ISO8601Date dateToISO8601Date(final long time) {
		return new ISO8601Date(new Date(time));
	}

	@Converter
	public static ISO8601Date dateToISO8601Date(final Date date) throws TransformerException {
		if (date == null) {
			throw new TransformerException("Can not tranform a null Date into an ISO8601Date.");
		}
		return new ISO8601Date(date);
	}


	@Converter
	public static ISO8601Date stringToISO8601Date(final String dateUsingISO8601format) throws TransformerException {
		if (dateUsingISO8601format == null) {
			throw new TransformerException("Can not tranform a null String into an ISO8601Date.");
		}
		try {
			return new ISO8601Date(dateUsingISO8601format);
		} catch (final IllegalArgumentException iae) {
			throw new TransformerException(iae);
		}
	}


}