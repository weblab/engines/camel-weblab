/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel.utils;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.NoTypeConversionAvailableException;
import org.apache.camel.TypeConverter;
import org.apache.camel.spi.TypeConverterRegistry;
import org.apache.commons.lang3.ArrayUtils;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.extended.exception.WebLabResourceCreationException;
import org.ow2.weblab.core.extended.ontologies.DCTerms;
import org.ow2.weblab.core.extended.ontologies.DublinCore;
import org.ow2.weblab.core.extended.ontologies.RDF;
import org.ow2.weblab.core.extended.ontologies.RDFS;
import org.ow2.weblab.core.extended.ontologies.WebLabModel;
import org.ow2.weblab.core.extended.ontologies.WebLabProcessing;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Segment;
import org.ow2.weblab.core.model.TemporalSegment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.Video;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.engine.camel.WeblabEndpoint;
import org.ow2.weblab.rdf.XMLParser;

/**
 * Dull work on inner WebLab objects
 *
 * @author asaval
 *
 */
public class WebLabUtils {


	private static enum Action {
		CREATE, READ, UPDATE, DELETE
	}


	public static final String WEBLAB_CONTENT_RESOURCE = "WEBLAB_CONTENT_RESOURCE";


	public static final String WEBLAB_CONTENT_URI = "WEBLAB_CONTENT_URI";


	private static ContentManager contentManager = null;


	public static <T> T parse(final Object data, final Class<T> clazz) throws Exception {
		if (clazz.isInstance(data)) {
			return (T) data;
		}
		final Method m = XMLParser.class.getDeclaredMethod("xmlParse", String.class, clazz.getClass());
		m.setAccessible(true);
		return (T) m.invoke(XMLParser.class, data.toString(), clazz);
	}


	/**
	 * Convert a primitive type to its object equivalent class
	 *
	 * @param primitiveClass
	 *            The class primitive class to convert
	 * @return The Object class
	 */
	public static Class<?> primitiveToObject(final Class<?> primitiveClass) {
		if (primitiveClass.equals(boolean.class)) {
			return Boolean.class;
		} else if (primitiveClass.equals(int.class)) {
			return Integer.class;
		} else if (primitiveClass.equals(long.class)) {
			return Long.class;
		} else if (primitiveClass.equals(double.class)) {
			return Double.class;
		} else if (primitiveClass.equals(float.class)) {
			return Float.class;
		} else if (primitiveClass.equals(char.class)) {
			return Character.class;
		} else if (primitiveClass.equals(short.class)) {
			return Short.class;
		} else if (primitiveClass.equals(byte.class)) {
			return Byte.class;
		}
		return primitiveClass;
	}


	/**
	 * Convert a primitive type array to its object equivalent class array
	 *
	 * @param data
	 *            the primitive array to convert
	 * @param clazz
	 *            the primitive type of the array
	 * @return An array of the same object value
	 */
	public static Object[] toObjectArray(final Object data, final Class<?> clazz) {
		Object[] array = null;
		if (int.class.equals(clazz)) {
			array = ArrayUtils.toObject((int[]) data);
		} else if (long.class.equals(clazz)) {
			array = ArrayUtils.toObject((long[]) data);
		} else if (boolean.class.equals(clazz)) {
			array = ArrayUtils.toObject((boolean[]) data);
		} else if (float.class.equals(clazz)) {
			array = ArrayUtils.toObject((float[]) data);
		} else if (double.class.equals(clazz)) {
			array = ArrayUtils.toObject((double[]) data);
		} else if (char.class.equals(clazz)) {
			array = ArrayUtils.toObject((char[]) data);
		} else if (short.class.equals(clazz)) {
			array = ArrayUtils.toObject((short[]) data);
		} else if (byte.class.equals(clazz)) {
			array = ArrayUtils.toObject((byte[]) data);
		}
		return array;
	}


	/**
	 * Transform data to a useful list
	 *
	 * @param data
	 *            the primitive array
	 * @param clazz
	 *            the type of the primitive datat
	 * @return A list of object
	 */
	public static List<?> toList(final Object data, final Class<?> clazz) {
		List<?> list;
		if (clazz.isArray()) {
			Object[] array;
			if (clazz.getComponentType().isPrimitive()) {
				array = WebLabUtils.toObjectArray(data, clazz.getComponentType());
			} else {
				array = (Object[]) data;
			}
			list = Arrays.asList(array);
		} else {
			list = (List<?>) data;
		}
		return list;
	}


	/**
	 * Creates segments depending of their type.
	 * Warning: support only LinearSegment and TemporalSegment
	 *
	 * @param segment
	 *            the class name of the segment type
	 * @param data
	 *            the list of intervals (array of int) for start and stop of the segments to be created
	 * @param itemsType
	 *            integer or long
	 * @return the list of created segments
	 * @throws ClassNotFoundException
	 *             If <code>segment</code> is not the canonical name of class to be loaded
	 * @throws IllegalArgumentException
	 *             If <code>segment</code> is something else than <code>org.ow2.weblab.core.model.TemporalSegment</code> or <code>org.ow2.weblab.core.model.LinearSegment</code>
	 */
	public static List<Segment> createSegments(final String segment, final Object data, final Class<?> itemsType) throws ClassNotFoundException, IllegalArgumentException {
		final List<Segment> segments = new LinkedList<>();

		if (data == null || segment == null) {
			return segments;
		}

		final Class<?> segmentClass = Class.forName(segment);

		final List<?> intervals = WebLabUtils.toList(data, itemsType);
		if (LinearSegment.class.equals(segmentClass)) {
			final Object[] array = intervals.toArray();
			for (int i = 0; i < array.length; i += 2) {
				final LinearSegment ls = new LinearSegment();
				ls.setStart((int) array[i]);
				ls.setEnd((int) array[i + 1]);
				ls.setUri("weblab://" + UUID.randomUUID().toString());
				segments.add(ls);
			}
		} else if (TemporalSegment.class.equals(segmentClass)) {
			final Object[] array = intervals.toArray();
			for (int i = 0; i < array.length; i += 2) {
				final TemporalSegment ls = new TemporalSegment();
				ls.setStart((int) array[i]);
				ls.setEnd((int) array[i + 1]);
				ls.setUri("weblab://" + UUID.randomUUID().toString());
				segments.add(ls);
			}
		} else {
			throw new IllegalArgumentException("Class " + segment + " not supported.");
		}
		return segments;
	}


	/**
	 * Give a WebLab type, return a WebLab resource
	 *
	 * @param type
	 *            The simple name of the Resource type (Document, Text, Image, Audio...) or a canonical name
	 * @return The created resource
	 * @throws InstantiationException
	 *             If <code>type</code> is not the name of class to be loaded
	 * @throws IllegalAccessException
	 *             If <code>type</code> is the name of class that has no public no parameters constructor
	 */
	public static Resource initResource(final String type) throws InstantiationException, IllegalAccessException {
		Resource resource = null;

		if (type.equalsIgnoreCase("Document")) {
			resource = new Document();
		} else if (type.equalsIgnoreCase("Text")) {
			resource = new Text();
		} else if (type.equalsIgnoreCase("Image")) {
			resource = new Image();
		} else if (type.equalsIgnoreCase("Audio")) {
			resource = new Audio();
		} else if (type.equalsIgnoreCase("Video")) {
			resource = new Video();
		} else if (type.equalsIgnoreCase("Resource")) {
			resource = new Resource();
		} else if (type.equalsIgnoreCase("Annotation")) {
			resource = new Annotation();
		} else if (type.equalsIgnoreCase("PieceOfKnowledge")) {
			resource = new PieceOfKnowledge();
		} else {
			// try to resolve the class
			try {
				final Class<?> clazz = Class.forName(type);
				if (Resource.class.isAssignableFrom(clazz)) {
					resource = (Resource) clazz.newInstance();
				}
			} catch (final ClassNotFoundException cnfe) {
				// we do not care
			} catch (final ClassCastException cce) {
				// should not happen since we checked clazz "assignability", anyway we still do not care
			}
			if (resource == null) {
				throw new WebLabResourceCreationException("The type '" + type + "' is not a valid weblab type");
			}
		}

		resource.setUri("weblab://" + UUID.randomUUID().toString());
		return resource;
	}


	public static URI writeContent(final Exchange exchange, final Resource resource, final Object content, final boolean filterNonXMLChars, final boolean writeContent)
			throws NoTypeConversionAvailableException, IOException {
		URI writtenURI = null;
		if (writeContent) {

			// writing content from body (only for image, text, audio, video)

			if (resource instanceof Text) {

				final String text = WebLabUtils.tryConvertTo(String.class, exchange, content);

				if (filterNonXMLChars) {
					((Text) resource).setContent(WebLabUtils.stripNonValidXMLCharacters(text));
				} else {
					((Text) resource).setContent(text);
				}
			}

			if (resource instanceof Image) {
				((Image) resource).setContent(WebLabUtils.tryConvertTo(byte[].class, exchange, content));
			}

			if (resource instanceof Audio) {
				((Audio) resource).setContent(WebLabUtils.tryConvertTo(byte[].class, exchange, content));
			}

			if (resource instanceof Video) {
				((Video) resource).setContent(WebLabUtils.tryConvertTo(byte[].class, exchange, content));
			}
		} else {
			// write body as native content (excepted for POK and Annotation)
			if (!(resource instanceof PieceOfKnowledge || resource instanceof Annotation)) {

				if (WebLabUtils.contentManager == null) {
					WebLabUtils.contentManager = ContentManager.getInstance();
				}

				writtenURI = WebLabUtils.contentManager.create(WebLabUtils.tryConvertTo(InputStream.class, exchange, content), resource, exchange.getIn().getHeaders());

				// annotate as native content
				final WProcessingAnnotator wpa = new WProcessingAnnotator(resource);
				wpa.writeNativeContent(writtenURI);

			}
		}
		return writtenURI;
	}


	/**
	 * Tries to convert the value to the specified type in the context of an exchange, throwing an exception if not possible to convert.
	 *
	 * @param toClass
	 *            the requested type
	 * @param exchange
	 *            the current exchange
	 * @param value
	 *            the value to convert, can be null
	 * @param <T>
	 *            the type to be returned
	 * @return the converted value or null if the value is null
	 * @throws NoTypeConversionAvailableException
	 *             If no type converter has been found
	 */
	public static <T> T tryConvertTo(final Class<T> toClass, final Exchange exchange, final Object value) throws NoTypeConversionAvailableException {

		if (value == null) {
			return null;
		}

		// check if value is already in a compatible class
		if (toClass.isInstance(value)) {
			return (T) value;
		}

		// else we try to convert the value
		final TypeConverterRegistry tcRegistry = exchange.getContext().getTypeConverterRegistry();
		final TypeConverter typeConvert = tcRegistry.lookup(toClass, value.getClass());
		if (typeConvert == null) {
			throw new NoTypeConversionAvailableException(value, toClass);
		}
		return typeConvert.tryConvertTo(toClass, exchange, value);
	}


	/**
	 * This method ensures that the output String has only valid XML unicode characters as specified by the XML 1.0 standard. For reference, please see <a
	 * href="http://www.w3.org/TR/2000/REC-xml-20001006#NT-Char">the standard</a>. This method will return an empty String if the input is null or empty.
	 *
	 * @param in
	 *            The String whose non-valid characters we want to remove.
	 * @return The in String, stripped of non-valid characters.
	 */
	public static String stripNonValidXMLCharacters(final String in) {
		final StringBuffer out = new StringBuffer(); // Used to hold the output.
		char current; // Used to reference the current character.

		if (in == null || "".equals(in)) {
			return ""; // vacancy test.
		}
		for (int i = 0; i < in.length(); i++) {
			current = in.charAt(i); // NOTE: No IndexOutOfBoundsException caught here; it should not happen.
			if (current == 0x9 || current == 0xA || current == 0xD || current >= 0x20 && current <= 0xD7FF || current >= 0xE000 && current <= 0xFFFD || current >= 0x10000 && current <= 0x10FFFF) {
				out.append(current);
			}
		}
		return out.toString();
	}


	/**
	 * Return the default WebLab supported prefixes
	 *
	 * @return The map of namespaces
	 */
	public static Map<String, String> defaultNamespace() {
		final Map<String, String> map = new HashMap<>();
		map.put("wlm", WebLabModel.NAMESPACE);
		map.put(WebLabProcessing.PREFERRED_PREFIX, WebLabProcessing.NAMESPACE);
		map.put("rdf", RDF.NAMESPACE);
		map.put("rdfs", RDFS.NAMESPACE);
		map.put("dc", DublinCore.NAMESPACE);
		map.put(DCTerms.PREFERRED_PREFIX, DCTerms.NAMESPACE);
		return map;
	}


	public static void manageContent(final URI endpointURI, final WeblabEndpoint endpoint, final Exchange exchange) throws CamelException, IOException {
		final String authority = endpointURI.getAuthority();
		final int index = authority.indexOf(':');
		String info = "";
		if (index != -1) {
			info = authority.substring(index + 1);
		}

		final Map<String, Object> params = exchange.getIn().getHeaders();
		final Resource resource = exchange.getIn().getHeader(WebLabUtils.WEBLAB_CONTENT_RESOURCE, Resource.class);
		final URI uri = exchange.getIn().getHeader(WebLabUtils.WEBLAB_CONTENT_URI, URI.class);

		if (WebLabUtils.contentManager == null) {
			WebLabUtils.contentManager = ContentManager.getInstance();
		}
		Object result = null;
		switch (Action.valueOf(info.toUpperCase())) {
			case CREATE:
				result = WebLabUtils.contentManager.create(exchange.getIn().getBody(InputStream.class), resource, params);
				break;
			case READ:
				result = WebLabUtils.contentManager.read(uri != null ? uri : exchange.getIn().getBody(URI.class), resource, params);
				break;
			case UPDATE:
				result = WebLabUtils.contentManager.update(exchange.getIn().getBody(InputStream.class), uri, resource, params);
				break;
			case DELETE:
				result = Boolean.valueOf(WebLabUtils.contentManager.delete(exchange.getIn().getBody(URI.class), resource, params));
				break;
			default:
				throw new CamelException("WebLab endpoint does not support " + info + " action to manage content");
		}
		if (exchange.hasOut()) {
			exchange.setOut(null);
		}
		exchange.getIn().setBody(result);
	}

}