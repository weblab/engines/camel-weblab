/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel.dataformat.date;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


/**
 * This class is a subclass of Date.
 * It is needed in order to create a dedicated converter to be used in camel routes.
 * It enables to convert String that follows the RFC 2616 specific date format into a Date.
 * Without the creation of this specific subclass, the standard conversion would be used.
 *
 * The patterns have been found in the DateUtils class of HTTP Client
 *
 *
 * @author ymombrun
 * @see "http://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.3.1"
 */
public class RFC2616Date {


	private static final TimeZone GMT = TimeZone.getTimeZone("GMT");


	/**
	 * Date format pattern used to parse HTTP date headers in RFC 1123 format.
	 */
	public static final String RFC_1123 = "EEE, dd MMM yyyy HH:mm:ss zzz";


	/**
	 * Date format pattern used to parse HTTP date headers in RFC 850 format (deprecated since it is missing the four digit year).
	 */
	public static final String RFC_850 = "EEE, dd-MMM-yy HH:mm:ss zzz";


	/**
	 * Date format pattern used to parse HTTP date headers in ANSI C <code>asctime()</code> format.
	 */
	public static final String ASCTIME = "EEE MMM d HH:mm:ss yyyy";


	private static final String[] RFC_2616_ALLOWED_PATTERNS = new String[] { RFC2616Date.RFC_1123, RFC2616Date.RFC_850, RFC2616Date.ASCTIME };


	private final long time;


	/**
	 * @param rfc2616Date
	 *            The date in one of the accepted formats in RFC2616.
	 * @throws ParseException
	 *             If the date is not valid against any of the patterns
	 */
	public RFC2616Date(final String rfc2616Date) throws ParseException {
		this(RFC2616Date.parseDate(rfc2616Date));
	}


	/**
	 * Decorates a date by an RFC2616Date
	 *
	 * @param date
	 *            A date to be decorated
	 */
	public RFC2616Date(final Date date) {
		this.time = date.getTime();
	}


	/**
	 * Uses UK as locale and GMT as Timezone
	 *
	 * @param rfc2616Date
	 *            The date to be parsed
	 * @return The date parsed
	 * @throws ParseException
	 *             If the date cannot be parsed using the RFC_2616_ALLOWED_PATTERNS
	 */
	public static Date parseDate(final String rfc2616Date) throws ParseException {
		for (final String format : RFC2616Date.RFC_2616_ALLOWED_PATTERNS) {
			final SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.UK);
			sdf.setTimeZone(RFC2616Date.GMT); // Mandatory in RFC 2616
			final ParsePosition pos = new ParsePosition(0);
			final Date parsedDate = sdf.parse(rfc2616Date, pos);
			if (parsedDate != null && pos.getIndex() == rfc2616Date.length()) {
				return parsedDate;
			}
		}
		throw new ParseException("Unable to parse the date: " + rfc2616Date, -1);
	}


	public long getTime() {
		return this.time;
	}

}
