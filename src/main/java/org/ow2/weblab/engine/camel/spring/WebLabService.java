/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.engine.camel.spring;

import java.util.Map;

/**
 * An useless but necessary WebLab Service bean.
 * @author asaval
 *
 */
public class WebLabService {

	private String address;
	private String id;
	private String cxfEndpointName;
	private String osgiServiceName;
	private Map<String, Object> cxfProperties;
	private Map<String, Object> osgiProperties;

	public WebLabService(String address) throws Exception{
		this.address = address;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCxfEndpointName() {
		return this.cxfEndpointName;
	}

	public void setCxfEndpointName(String cxfEndpointName) {
		this.cxfEndpointName = cxfEndpointName;
	}

	public String getOsgiServiceName() {
		return this.osgiServiceName;
	}

	public void setOsgiServiceName(String osgiServiceName) {
		this.osgiServiceName = osgiServiceName;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Map<String, Object> getCxfProperties() {
		return this.cxfProperties;
	}

	public void setCxfProperties(Map<String, Object> cxfProperties) {
		this.cxfProperties = cxfProperties;
	}

	public Map<String, Object> getOsgiProperties() {
		return this.osgiProperties;
	}

	public void setOsgiProperties(Map<String, Object> osgiProperties) {
		this.osgiProperties = osgiProperties;
	}

}