<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:analyser="http://weblab.ow2.org/core/1.2/services/analyser" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">					
					
	<xsl:output method="xml" version="1.0"  indent="yes"/>
	
	<xsl:template match="/analyser:processReturn">
		<xsl:variable name="WLprefix" select="substring-before(resource/@xsi:type, ':')"/>
		<xsl:element name="resource">
			<xsl:namespace name="{$WLprefix}">http://weblab.ow2.org/core/1.2/model#</xsl:namespace>
			<xsl:for-each select="resource/@*">
				<xsl:copy/>
			</xsl:for-each>
			<xsl:copy-of select="resource/*"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>