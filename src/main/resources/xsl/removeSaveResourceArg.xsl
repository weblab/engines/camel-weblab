<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:resourceContainer="http://weblab.ow2.org/core/1.2/services/resourcecontainer" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">					
					
	<xsl:output method="text" version="1.0"  indent="yes"/>
	
	<xsl:template match="/resourceContainer:saveResourceReturn">
		
		<xsl:copy-of select="resourceId"/>
		
	</xsl:template>
</xsl:stylesheet>