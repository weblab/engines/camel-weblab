<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:xalan="http://xml.apache.org/xalan" xmlns:xslt="http://xml.apache.org/xslt" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:modelns="http://weblab.ow2.org/core/1.2/model#"
					xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
					xmlns:dc="http://purl.org/dc/elements/1.1/"
					xmlns:dct="http://purl.org/dc/terms/"
					xmlns:twirl="http://twirl.org/ontology#"
					xmlns:yweather="http://xml.weather.yahoo.com/ns/rss/1.0"
					xmlns:geo="http://www.w3.org/2003/01/geo/wgs84_pos#"
					xmlns:fn="http://www.w3.org/2005/xpath-functions"
					xmlns:uuid="xalan://java.util.UUID"
					xmlns:x="http://www.w3.org/1999/xhtml"
					xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
					xmlns:trainable="http://weblab.ow2.org/core/1.2/services/trainable"
					xmlns:datetime="http://exslt.org/dates-and-times"
					xmlns:xs="http://www.w3.org/2001/XMLSchema">					
					
	<xsl:output method="xml" version="1.0" indent="yes" xslt:indent-amount="4" encoding="utf-8"/>

	<xsl:template match="/">	
		<xsl:element name="trainable:addTrainResourceArgs">
			<xsl:copy-of select="resource"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>