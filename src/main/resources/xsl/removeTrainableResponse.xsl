<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:trainable="http://weblab.ow2.org/core/1.2/services/trainable">					
					
	<xsl:output method="xml" version="1.0"  indent="yes"/>
	
	<!-- operation : train -->
	<xsl:template match="/trainable:trainResponse">
		<xsl:variable name="WLprefix" select="substring-before(resource/@xsi:type, ':')"/>
		<xsl:element name="resource">
			<xsl:namespace name="{$WLprefix}">http://weblab.ow2.org/core/1.2/model#</xsl:namespace>
			<xsl:for-each select="resource/@*">
				<xsl:copy/>
			</xsl:for-each>
			<xsl:copy-of select="resource/*"/>
		</xsl:element>
	</xsl:template>
	
	<!-- operation : addTrainResource -->
	<xsl:template match="/trainable:addTrainResourceResponse">
		<xsl:variable name="WLprefix" select="substring-before(resource/@xsi:type, ':')"/>
		<xsl:element name="resource">
			<xsl:namespace name="{$WLprefix}">http://weblab.ow2.org/core/1.2/model#</xsl:namespace>
			<xsl:for-each select="resource/@*">
				<xsl:copy/>
			</xsl:for-each>
			<xsl:copy-of select="resource/*"/>
		</xsl:element>
	</xsl:template>
	
	<!-- operation : resetTrainedModel -->
	<xsl:template match="/trainable:resetTrainedModelResponse">
		<xsl:variable name="WLprefix" select="substring-before(resource/@xsi:type, ':')"/>
		<xsl:element name="resource">
			<xsl:namespace name="{$WLprefix}">http://weblab.ow2.org/core/1.2/model#</xsl:namespace>
			<xsl:for-each select="resource/@*">
				<xsl:copy/>
			</xsl:for-each>
			<xsl:copy-of select="resource/*"/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>